<?php


namespace App\Http\Controllers;


use App\Http\Requests\ProfileData;
use App\Http\Requests\ShopData;
use App\Http\Requests\UserForntendLogin;
use App\Http\Requests\UserForntendRegister;
use App\Models\AddressModel;
use App\Models\CustomerModel;
use App\Models\OrdersModel;
use App\Models\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;


class HomeController extends BaseController
{

    public function homepage(){
        return view('home.main');
    }
    public function logout(){
        session()->flush();
        return redirect(route('home'));
    }
    public function aboutus(){
        return view('home.aboutus');
    }
    public function contactus(){
        return view('home.contactus');
    }
    public function login(Request $r){
        $rData=$r->only(['to']);

        return view('home.login',['rData'=>$rData]);
    }

    public function loginpost(UserForntendLogin $r){
        $input=$r->validated();


        $model=new CustomerModel();

        $foundUser=$model->orWhere([
            'email'=>$input['username'],
            'mobile'=>$input['username'],
        ])->get();

        if($foundUser->count()<1)return response()->json(['errors'=>['username'=>['Please provide valid Email or Mobile No.']]],422);
        $foundUser=$foundUser->first()->toArray();

        if(!($foundUser['password']===$input['password']))return response()->json(['errors'=>['username'=>['Please provide valid Credentials.'],'password'=>['Password is case sensitive.']]],422);

        auth()->guard('customer')->loginUsingId($foundUser['id']);

        return response()->json(['ResponseResult'=>true,'ResponseData'=>$this->makeSafeUser(auth()->guard('customer')->user(),['password'])]);

    }

    public function register(){
        return view('home.register');
    }

    public function registerpost(UserForntendRegister $r){
        $input=$r->validated();

        $newCustomer=[
            'name'=>strtolower(implode(' ',[$input['fname'],$input['lname']])),
            'imei'=>0,
            'email'=>strtolower($input['email']),
            'ccode'=>'+91',
            'mobile'=>0,
            'rdate'=>now(),
            'password'=>$input['password'],
            'status'=>1,
            'pin'=>0
        ];

        CustomerModel::create($newCustomer);

        $foundUser=CustomerModel::where(['email'=>$input['email']])->get()->first();

        auth()->guard('customer')->loginUsingId($foundUser->id);



        return response()->json(['ResponseResult'=>true,'ResponseData'=>$this->makeSafeUser(auth()->guard('customer')->user(),['password','mobile','rdate'])]);


    }

    private function makeSafeUser($data=[],$unset=[]) {

        if(!is_array($data))$data=$data->toArray();
        if(count($unset) &&  count($data)){
            foreach ($unset as $k)if(array_key_exists($k,$data))unset($data[$k]);
        }

        return $data;
    }

    public function cart(){
        return view('home.cart');
    }

    public function shop(){
        return view('home.shop');
    }
    public function productDetails($id){


        $foundProduct=ProductModel::where(['id'=>$id]);

        if($foundProduct===null)return abort('404');
        $foundProduct=$foundProduct->first()->toArray();



      //  dd($foundProduct);

        $data=[
            'product'=>$foundProduct
        ];

        return view('home.product_details',$data);
    }

    public function shopData(ShopData  $r){


        $input=$r->validated();

        $perPage=(array_key_exists('per_page',$input))?$input['per_page']:12;
        $page=(array_key_exists('current_page',$input))?$input['current_page']:1;
        //dd($page);
        $where=[];
        $whereLike=[];
        if(array_key_exists('filter_by',$input) && is_array($input['filter_by']) && count($input['filter_by']))foreach ($input['filter_by'] as $c){
            $where[$c['columnName']]=$c['coumnValue'];
        }

        if(array_key_exists('q',$input) && strlen($input['q'])){
            $whereLike['pname']=$input['q'];
            $where['sname']=$input['q'];
            $where['psdesc']=$input['q'];
        }


        $model=ProductModel::where($where);

        if(count($whereLike)){
            foreach ($whereLike as $c=>$v){
                $model=$model->orWhere($c,'like',implode('',['%',$v,'%']));
            }
        }


        $resData=[

            'ResponseResult'=>true,
            'ResponseData'=>$model->paginate($perPage,["*"],'page',$page)

        ];



        return response()->json($resData);





    }


    public function userProfile(ProfileData $r){

        $loggedInUser=auth()->guard('customer')->user()->toArray();
        $vData=$r->validated()??[];

        return view('home.userprofile',['user'=>$loggedInUser,'data'=>$vData]);



    }

    public function userProfileSave(Request $r){

        $input=$r->only(['current_password','password','password_confirmation','email','fname','lname','mobile']);


        $message=[];
        $cUser=auth()->guard('customer')->user()->toArray();
        if(array_key_exists('current_password',$input)){
            $input=array_filter($input);
            if(count($input)!=3) {
                $resData=[
                    'ResponseResult'=>'false',
                    'ResponseData'=>'all input field are required',
                    'ResponseCode'=>422

                ];
                return ($resData['ResponseCode']===200)?response()->json($resData,200):response()->json($resData,422);
            }

            if($input['password'] != $input['password_confirmation']) {
                $resData=[
                    'ResponseResult'=>'false',
                    'ResponseData'=>'password confirmation did not matched',
                    'ResponseCode'=>422

                ];
                return ($resData['ResponseCode']===200)?response()->json($resData,200):response()->json($resData,422);
            }

            if($cUser['password']!==$input['current_password']) {
                $resData=[
                    'ResponseResult'=>'false',
                    'ResponseData'=>'current password not matched with our records',
                    'ResponseCode'=>422

                ];
                return ($resData['ResponseCode']===200)?response()->json($resData,200):response()->json($resData,422);
            }

            $newData=[
                'password'=>$input['password']
            ];

            CustomerModel::where(['id'=>$cUser['id']])->update($newData);
            $resData=[
                'ResponseResult'=>'true',
                'ResponseData'=>'password has been changed,signing out',
                'ResponseCode'=>200,

            ];





        }else{


            if(array_key_exists('lname',$input) && array_key_exists('lname',$input)){
                $input['name']=implode(' ',[$input['fname'],$input['lname'] ]);
                unset($input['fname']);
                unset($input['lname']);
            }
            foreach ($input as $k=>$in){

                if( array_key_exists($k,$cUser) &&  strtolower($in)== strtolower($cUser[$k]) )unset($input[$k]);
            }

            if(count($input)>0)CustomerModel::where(['id'=>$cUser['id']])->update($input);
            $message=(count($input)>0)?['status'=>true, 'message'=>'Profile Updated']:['status'=>false, 'message'=>'nothing to Update'];

            if(array_key_exists('status',$message) && $message['status']){
                $resData=[
                    'ResponseResult'=>$message['status'],
                    'ResponseData'=>$message['message'],
                    'ResponseCode'=>200
                ];
            }else {
                $resData=[
                    'ResponseResult'=>$message['status'],
                    'ResponseData'=>$message['message'],
                    'ResponseCode'=>422

                ];
            }

        }



        return ($resData['ResponseCode']===200)?response()->json($resData,200):response()->json($resData,422);




    }

    protected $d="$;";

    public function orderDetails($id,Request $r){

        $foundOrder=OrdersModel::find($id);
        if($foundOrder===null)abort('404');

        $foundUser=CustomerModel::find($foundOrder['uid']);
        if($foundUser===null)abort('404');
        $foundUser=$foundUser->toArray();

        $foundAddress=AddressModel::find($foundOrder['address_id']);

        if($foundAddress===null)abort('404');

        $foundAddress=$foundAddress->toArray();


        $foundOrder=$foundOrder->toArray();
        //dd($foundOrder);
        $pNameExplode=explode($this->d,$foundOrder['pname']);
        $pIdExplode=explode($this->d,$foundOrder['pid']);
        $pTypeExplode=explode($this->d,$foundOrder['ptype']);
        $pPriceExplode=explode($this->d,$foundOrder['pprice']);
        $pQtExplode=explode($this->d,$foundOrder['qty']);
        $orderFinal=[];
        foreach ($pNameExplode as $k=>$v){
            $findProduct=ProductModel::find($pIdExplode[$k]);
            if($findProduct===null)abort('404');
            $findProduct=$findProduct->toArray();
            $orderFinal['products'][]=[
                'name'=>$pNameExplode[$k],
                'id'=>$pIdExplode[$k],
                'type'=>$pTypeExplode[$k],
                'price'=>$pPriceExplode[$k],
                'pqt'=>$pQtExplode[$k],
                'subtotal'=>$pQtExplode[$k]*$pPriceExplode[$k],
                'more'=>$findProduct
            ];


        }


        $collectionProduct=collect($orderFinal['products']);


        $orderFinal['status']=$foundOrder['status'];
        $orderFinal['oid']=$foundOrder['oid'];
        $orderFinal['id']=$foundOrder['id'];
        $orderFinal['p_method']=$foundOrder['p_method'];
        $orderFinal['tax']=$foundOrder['tax'];
        $orderFinal['photo']=$foundOrder['photo'];
        $orderFinal['address']=$foundAddress;
        $orderFinal['user']=$foundUser;
        $orderFinal['total']=$foundOrder['total'];
        $orderFinal['order_date']=$foundOrder['order_date'];
        $orderFinal['ptotal']=$collectionProduct->sum(function ($product) {
            return $product['price']*$product['pqt'];
        });


        return view('home.orderview',['order'=>$orderFinal]);

    }

    public function checkoutCart(Request $r){

        if(!auth()->guard('customer')->check())abort('403');
        $foundUser=auth()->guard('customer')->user()->toArray();
      //  dd($foundUser);
        $address=$foundUser['address'];

        return view('home.checkout',['address'=>$address]);


    }

    public function orderCancel($id)
    {
        if (!auth()->guard('customer')->check()) abort('403');
        $foundUser = auth()->guard('customer')->user()->toArray();
        $foundOrderM = OrdersModel::find($id);
        $foundOrder = $foundOrderM;
        if ($foundOrder === null) abort('404');
        $foundOrder = $foundOrder->toArray();
        $foundOrderM->update(['status' => 'cancelled']);
        $foundOrderM->save();
        return redirect(route('orderDetails',['id'=>$id]));
    }

    public function checkoutPayment (){



    }

    public function checkoutPaymentPost (Request $r){

        $input=$r->only(['cart','address']);
       // dd($input);

        $foundUser=auth()->guard('customer')->user();

        dd($foundUser);
        $cart=$input['cart'];
        $daystodeliver=4;


        //dd(now()->addDays($daystodeliver)->format('--d-m-Y'));
        $cartCollection=collect($cart);
        $d="$;";
        $pname=implode($d,$cartCollection->pluck('pname')->toArray());
        $pid=implode($d,$cartCollection->pluck('id')->toArray());
        $pproce=$cartCollection->pluck('price')->toArray();
        $pprice=implode($d,reset($pproce));
        $qty=implode($d,$cartCollection->pluck('qt')->toArray());

       // dd($cartCollection  );
        //$address_id=
      //  dd($pprice);



        $order=[
//
            'oid'=>Str::random(5),
//            'uid'=>,
            'pname'=>$pname,
            'pid'=>$pid,
            'pprice'=>$pprice,
            'ddate'=>now()->addDays($daystodeliver)->format('--d-m-Y'),
//            'timesloat'=>,
            'order_date'=>now()->format('Y-m-d'),
            'status'=>"pending",
            'qty'=>$qty,
//            'total'=>,
//            'rate'=>,
//            'p_method'=>,
//            'rid'=>,
//            'a_status'=>,
//            'photo'=>,
//            's_photo'=>,
//            'r_status'=>,
//            'pickup'=>,
            'address_id' => $input['address']['id'],
//            'tid'=>,


        ];


        dd($order);


    }
}
