<?php

namespace App\Http\Requests;

use App\Models\CustomerModel;
use Illuminate\Foundation\Http\FormRequest;

class UserForntendRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'=>['filled'],
            'lname'=>['filled'],
            'email'=>['filled','email','unique:user,email'],
            'password'=>['filled','confirmed'],
            'password_confirmation'=>['filled'],
        ];
    }

    public function attributes()
    {
     return[
         'fname'=>'First Name',
         'lname'=>'Last Name',
         'email'=>'Email',
         'password'=>'Password',
     ];
    }


}
