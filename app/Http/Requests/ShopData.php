<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ShopData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $r)
    {
        return $r->ajax();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                    'current_page'=>[],
                    'last_page'=>[],
                    'per_page'=>[],
                    'q'=>[],
                    'sort_by'=>[],
                    'filter_by'=>[],

        ];
    }
}
