<?php


namespace App\Models;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;

class CustomerModel extends \Illuminate\Foundation\Auth\User
{

    use ValidatesRequests;

    protected $fillable=[
        'name',
        'imei',
        'email',
        'ccode',
        'mobile',
        'rdate',
        'password',
        'status',
        'pin',
    ];

    protected $hidden=[
        'imei'

    ];

    public $timestamps = false;

    protected $table="user";
    protected $appends=[];
    protected $with=['address'];

    public function address()
    {
        return $this->hasMany(AddressModel::class, 'uid', 'id');

    }
}
