<?php

namespace App\Models;

class CatModel extends \Illuminate\Database\Eloquent\Model{

    protected $table="category";

    protected $with=['subs'];

    public function subs(){
        return $this->hasMany(SubCatModel::class,'cat_id','id');
    }


}
