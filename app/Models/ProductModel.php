<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{

    protected $table="product";
    protected $appends=['price','units','default_price','default_unit','base_url','pimgs','view_url'];
    protected $dates=[
        'date'
    ];

    protected function getPriceAttribute(){
        return explode('$;',$this->pprice);
    }
    protected function getViewUrlAttribute(){
        return  route('productDetails',['id'=>$this->id]);

    }
    protected function getPimgsAttribute(){
        return explode(',',$this->prel);
    }
    protected function getUnitsAttribute(){
        return explode('$;',$this->pgms);
    }

    protected function getDefaultPriceAttribute(){
        return $this->price[0];
    }

    protected function getDefaultUnitAttribute(){
        return $this->units[0];
    }
     protected function getBaseUrlAttribute(){
            //return 'http://192.168.29.16:8083/';
            //return 'http://'.env('APP_MAIN_DOMAIN','192.168.0.111'). ':8083/';
         return $this->makeUrl();
        }


        private function makeUrl(){

        $url=implode('',[env('APP_BACKEND_DOMAIN_TYPE','http://'),implode(':',[env('APP_BACKEND_DOMAIN','192.168.0.111'),env('APP_BACKEND_DOMAIN_PORT','8083')]),'/']);

        return $url;
        }


}
