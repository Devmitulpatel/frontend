<?php

namespace App\Models;

class SubCatModel extends \Illuminate\Database\Eloquent\Model{

    protected $table="subcategory";


    protected $withCount=['products'];

    public  function products(){
        return $this->hasMany(ProductModel::class,'sid','id');
    }

}
