<?php

namespace App\Models;

class OrdersModel extends \Illuminate\Database\Eloquent\Model{

    protected $table="orders";

    protected $fillable=['*','status'];

    public $timestamps = false;

}
