<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class,'homepage'])->name('home');
Route::get('/about-us', [\App\Http\Controllers\HomeController::class,'aboutus'])->name('aboutus');
Route::get('/contact-us', [\App\Http\Controllers\HomeController::class,'contactus'])->name('contactus');
Route::get('/login', [\App\Http\Controllers\HomeController::class,'login'])->name('login');
Route::post('/login', [\App\Http\Controllers\HomeController::class,'loginpost']);
Route::get('/logout', [\App\Http\Controllers\HomeController::class,'logout'])->name('logout');
Route::get('/register', [\App\Http\Controllers\HomeController::class,'register'])->name('register');
Route::post('/register', [\App\Http\Controllers\HomeController::class,'registerpost']);
Route::get('/cart', [\App\Http\Controllers\HomeController::class,'cart'])->name('cart');
Route::get('/shop', [\App\Http\Controllers\HomeController::class,'shop'])->name('shop');
Route::post('/shop/data', [\App\Http\Controllers\HomeController::class,'shopData'])->name('shopData');
Route::get('/product/{id}', [\App\Http\Controllers\HomeController::class,'productDetails'])->name('productDetails');
Route::get('/my-profile', [\App\Http\Controllers\HomeController::class,'userProfile'])->name('userProfile');
Route::post('/my-profile', [\App\Http\Controllers\HomeController::class,'userProfileSave']);


Route::get('/order/{id?}', [\App\Http\Controllers\HomeController::class,'orderDetails'])->name('orderDetails');
Route::get('/order/cancel/{id?}', [\App\Http\Controllers\HomeController::class,'orderCancel'])->name('orderCancel');
Route::get('/checkout', [\App\Http\Controllers\HomeController::class,'checkoutCart'])->name('checkout');
Route::post('/checkout/payments', [\App\Http\Controllers\HomeController::class,'checkoutPaymentPost']);
Route::get('/checkout/payments', [\App\Http\Controllers\HomeController::class,'checkoutPayment'])->name('checkoutPayment');






Route::post('/test/for/all/app',function (\Illuminate\Http\Request $r){
    $input=$r->only('package_name');
    if(!array_key_exists('package_name',$input) || (array_key_exists('package_name',$input) && strlen($input['package_name'])<1 ))goto MError;


    $data=[

       'data'=> [


            [
                'id'=>'1',
                'app_account'=>'VOODUU',
                'app_name'=>'Beach Clean',
                'app_packagename'=>'com.ClassyTechnosoft.BeachClean',
                'app_icon'=>'https://lh3.googleusercontent.com/tl7wrjzqxWwLcQ3AOQOD9ePJEIpfc_JyRbQcHj0vI7KihoysZsHtL_S_MRivsKnTGQ=s180-rw',
                'app_category'=>'Game',
                'app_label'=>'New',
                'app_type'=>'Top',
                ],
        [
                'id'=>'2',
                'app_account'=>'VOODUU',
                'app_name'=>'Beach Clean',
                'app_packagename'=>'com.ClassyTechnosoft.BeachClean',
                'app_icon'=>'https://lh3.googleusercontent.com/tl7wrjzqxWwLcQ3AOQOD9ePJEIpfc_JyRbQcHj0vI7KihoysZsHtL_S_MRivsKnTGQ=s180-rw',
                'app_category'=>'Game',
                'app_label'=>'New',
                'app_type'=>'Top',
                ]



        ],

       'current_app' =>[
        'app_name'=>'test name',
        'pck_name'=>'test.name.here',
        'account_name'=>'test@gmail.cpom,',
        'more_app'=>url('/'),
        'privacy_policy'=>url('/'),
        'rate_us'=>url('/'),
        'custome_ad'=>[[

                            'app_name'=>'test app 1',
                            'app_icon'=>'test app 1',
                            'app_short_decription'=>'test app 1',
                            'ad_app_url'=>'test app 1',
                        ]],
        'update_custom'=>'ad',
        'banner'=>[
            'type'=>'admod',
            'id'=>'kjdhfk',
            ],
        'inter'=>[
            'type'=>'admod',
            'id'=>'kjdhfk',
        ],
        'reward'=>[
            'type'=>'facebook',
            'id'=>'kjdhfk',
        ],
        'native'=>[
            'type'=>'facebook',
            'id'=>'kjdhfk',
        ],
        ]

    ];





    if(false){
        MError:
        $data=[
            'ResponseResult'=>false,
            'ResponseData'=>['package_name'=>'Package Name Required'],
            'ResponseCode'=>422,

        ];
    }




    return response()->json($data);





});
