import Vue from 'vue';

Vue.component('shop-front',{

        props:['categories','latestProduct','startProductData','dataUrl'],

        data () {
            return {

                currentCat:[],
                productCollection:[],
                pageData:{
                    current_page:1,
                    last_page:1,
                    first_page_url:'',
                    last_page_url:'',
                    per_page:12,
                    from:1,
                    to:1,
                    next_page_url:'',
                    prev_page_url:'',
                    total:0
                },
                search_query:'',
                sortBy:[],
                filterBy:[],




            };
        },
        methods:{

            freshData(){
              var old= this.productCollection;
              // this.productCollection=null;
               //this.productCollection=old;
            },

            fireRootFunction(n,m){

                if(window.hasOwnProperty('VueApp')) {

                    var r =VueApp[n](m);
                    this.freshData();
                    return r;
                }else {
                    this.$root[n](m)

                }

            },
            productExistInCart(p){
                //console.log(this.fireRootFunction('productExistInCart',p));
               return  this.fireRootFunction('productExistInCart',p);
            },
            getAProduct(p){
                //console.log(this.fireRootFunction('productExistInCart',p));
               return  this.fireRootFunction('getAProduct',p);
            },
            remoceQtCart(p){
               return  this.fireRootFunction('remoceQtCart',p);
            },

            deleteFromCart(p){
               return  this.fireRootFunction('deleteFromCart',p);
            },


            addToCart(p){
                //var r=this.fireRootFunction('addToCart',p);

               return  this.fireRootFunction('addToCart',p);
            },

            nextPage(){
                if(this.pageData.current_page+1<=this.pageData.last_page)this.changePage(this.pageData.current_page+1);
            },
            previosPage(){
                if(this.pageData.current_page-1>0)this.changePage(this.pageData.current_page-1);
            },
            filterByCatNSubCat(c,sc){
                // console.log(c);
                // console.log(sc);
                this.filterBy=null;
                this.filterBy=[];
                var th=this;
                Vue.nextTick(function () {
                    th.filterBy.push({
                            columnName:'cid',
                            coumnValue:c
                        }
                    );
                    th.filterBy.push({
                            columnName:'sid',
                            coumnValue:sc
                        }
                    );
                })

                th.changeData();


            },

            searchProduct(){
                var th=this;
                th.changeData();
            },

            getDataFromServer(res){
            var pageData=res.data.ResponseData??this.pageData;
            this.pageData={};
            this.pageData=pageData;
            this.productCollection=null;
            this.productCollection=pageData.data;
            },
            getErrorFromServer(e){
                alert('Opps...Sorry We have some error,Please Reload the page and try again');
                console.log(e);
            },

            async changePage(p){

            if(this.pageData.current_page!=p){
                var th = this;
                var objectDataToPost = th.pageData;
                objectDataToPost.current_page=p;
                objectDataToPost.q=th.search_query;
                objectDataToPost.sort_by=th.sortBy;
                objectDataToPost.filter_by=th.filterBy;
                th.pageData=null;
                th.pageData=objectDataToPost;
                var data=await th.changeData(false);


            }

        },
        async changeData(forcedPageChanged=true){


                var th = this;
                var url =this.dataUrl;
                var objectDataToPost = this.pageData;
                if(forcedPageChanged)objectDataToPost.current_page=1;
                objectDataToPost.q=this.search_query;
                objectDataToPost.sort_by=th.sortBy;
                objectDataToPost.filter_by=th.filterBy;
                var c=window.axios;
                var data=await c.post(url,objectDataToPost).then(res=>th.getDataFromServer(res)).catch(e=>th.getErrorFromServer(e));



        },

            resetProduct(){
                this.productCollection=this.startProductData.data;
            }

    },
    created() {

            this.currentCat=this.categories;
            if(this.productCollection.length<1)this.productCollection=this.startProductData.data;
            this.pageData=
                {
                        current_page:this.startProductData.current_page,
                        last_page:this.startProductData.last_page,
                        first_page_url:this.startProductData.first_page_url,
                        last_page_url:this.startProductData.last_page_url,
                        per_page:this.startProductData.per_page,
                        from:this.startProductData.from,
                        to:this.startProductData.to,
                        next_page_url:this.startProductData.next_page_url,
                        prev_page_url:this.startProductData.prev_page_url,
                        total:this.startProductData.total
                };


    }
        ,
    computed:{



    },

    watch:{

    }


}
    );
