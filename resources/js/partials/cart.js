import Vue from 'vue';

Vue.component('mcart', {
        data: function () {
            return {
                currentCart: [],
                urlbase: 'http://b.rmart.m/'

            }
        },
        methods: {

            findProductFromId(x, p) {
                return x.id == p.id
            },
            addFromHome(p) {
                if (this.currentCart === null) this.currentCart = [];
                var productFoundIndex = this.currentCart.findIndex(x => this.findProductFromId(x, p));

                if (productFoundIndex === -1) {
                    p.qt = 1;
                    this.currentCart.push(p);
                } else {
                    this.currentCart[productFoundIndex].qt = this.currentCart[productFoundIndex].qt + 1;

                }


            },
            removeFromCart(key) {
                window.VueApp.removeFromCart(key)
            },
            removeFrom(key) {
                this.currentCart.splice(key, 1);
            },

            updateCartFromOthers(c) {
                this.currentCart = [];
                this.currentCart = c;

            }

        },
        computed: {

            totalAmount() {
                var total = 0;

                for (var i in this.currentCart) {
                    var p = parseFloat(this.currentCart[i].default_price);
                    var d = parseFloat(this.currentCart[i].discount);


                    total = total + this.currentCart[i].qt * (p - ((d / 100) * p));
                    //  console.log(['No : ',i, 'Price : ',p,'discount rate : ',d,'Q : ', this.currentCart[i].qt,'Discount Final : ',p - ( (d/100)*p ),'c Total : ',total].join(' ') );
                }
                if (window.hasOwnProperty('VueApp')) window.VueApp.updateMasterCart(this.currentCart);

                return total;
            }

        }


    }
);
