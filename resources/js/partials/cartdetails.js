import Vue from 'vue';

Vue.component('mcart-detailed', {
        props:['cart'],
        data: function () {
            return {
                urlbase: 'http://b.rmart.m/',
                currentCart: this.cart,
                shipping:50,
                taxrate:18

            }
        },
        mounted() {
            var th = this;
            Vue.nextTick(function () {
                if (window.hasOwnProperty('VueApp')) th.currentCart = window.VueApp.getCurrentCart();
            });


        },
        methods: {

            updateCartLocal(){
                var old=this.currentCart;
                this.currentCart=[];
                this.currentCart=old;
            },
            getAProduct(p){
                return window.VueApp.getAProduct(p)
            },
            remoceQtCart(p) {
                window.VueApp.remoceQtCart(p);
                this.updateCartLocal();

            },
            deleteFromCart(p) {

                window.VueApp.deleteFromCart(p);
                this.updateCartLocal();
            },
            addToCart (p) {

                window.VueApp.addToCart (p);
                this.updateCartLocal();
            }

        },
        watch:{
          cart(v){
              this.currentCart=[];
              this.currentCart=v;

          }
        },
        computed: {

            totalAmount(){
                var total=0;

                for (var i in this.currentCart){
                    var p=parseFloat(this.currentCart[i].default_price);
                    var d=parseFloat(this.currentCart[i].discount);



                    total=total+this.currentCart[i].qt*(  p - ( (d/100)*p )  );
                    //    console.log(['No : ',i, 'Price : ',p,'discount rate : ',d,'Q : ', this.currentCart[i].qt,'Discount Final : ',p - ( (d/100)*p ),'c Total : ',total].join(' ') );
                }


                return total;
            }
        }
    }
);
