import Vue from 'vue';
import Toasted from "vue-toasted";

Vue.component('order-list',{
        props:['mData','url','logout'],
        data() {
            return {

                form:{
                    email:'',
                    fname:'',
                    lname:'',
                    mobile:'',

                },

                toPassword:false,
            //     WaitingForResponseFromServer:false,
            //     currentLoadidText:1,
            //     loading:[],
            //     errors: {},
            //     words:
            //
            //         {
            //         1:    ['P','l', 'e', 'a', 's', 'e ', 'W', 'a', 'i', 't ', '.', '.', '.',' ',' ',' ',' ',' ',' '],
            //         2:    ['W', 'e ', 'a', 'r', 'e ', 'f', 'e', 't', 'c', 'h', 'i','n', 'g ','d','a','t','a ','f','r','o','m ','s','e','r','v','e','r','.','.','.',' ',' ',' ',' ',' ',' ']
            //          },
            //
            }
        },
    methods:{


        toggleModelOrderView(order){



            window.location.href=[this.url.viewOrder,order.id].join('/');


            console.log(order);

        },

            DataFromServer(res){

                var data = res.data.ResponseData;

                var th=this;
                this.$toasted.success(data,{ icon : 'check'});
                if(this.toPassword){
                    setTimeout(window.location.replace (th.logout), 5000);
                }else{
                    setTimeout(location.reload(true), 500);
                }






            },
            ErrorFromServer(e){
                var data = e.response.data.ResponseData;

                this.$toasted.error(data,{ icon : 'ban'});
            },

            async saveData(){

                var url=this.url;
                var client=axios;

                var data=await client.post(url,  this.form).then(res=>this.DataFromServer(res)).catch(e=>this.ErrorFromServer(e));

            },
            changePasswordClick1(){
                if(!this.toPassword) {
                    this.toPassword = true;
                    this.form={
                        current_password:'',
                        password:'',
                        password_confirmation:'',


                    };

                }
            },
            backToProfileEdit(){
                if(this.toPassword) {
                    this.toPassword = false;
                    this.form={
                        email:'',
                        fname:'',
                        lname:'',
                        mobile:'',

                    };
                }
            }
    },
    created() {
        //this.form={};
        this.form=this.mData;
        //console.log(this.mData);
     //   setInterval(this.loadingFunction, 50);

    }
        ,
    computed:{



    },

    watch:{

    }


}
    );
