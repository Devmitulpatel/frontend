import Vue from 'vue';

Vue.component('login-form',{

        props:['alreadyLoggedIn','rData'],

        data:function () {
            return {

                form:{
                    username:'',
                    password:'',
                    remember:false
                },
                WaitingForResponseFromServer:false,
                currentLoadidText:1,
                loggedIn:false,
                loading:[],
                errors: {},
                words:

                    {
                    1:    ['P','l', 'e', 'a', 's', 'e ', 'W', 'a', 'i', 't ', '.', '.', '.',' ',' ',' ',' ',' ',' '],
                    2:    ['W', 'e ', 'a', 'r', 'e ', 'f', 'e', 't', 'c', 'h', 'i','n', 'g ','d','a','t','a ','f','r','o','m ','s','e','r','v','e','r','.','.','.',' ',' ',' ',' ',' ',' ']
                     },

            }
        },
    methods:{

        redirectToHome(url=null){
            window.location.href=url??"/";
        },
        async loginPress(url){


            if(!this.WaitingForResponseFromServer){
                var th = this;
                th.WaitingForResponseFromServer = true;
                th.errors = {};

                var data = await window.axios.post(url, this.form).then(function (res) {
                    th.WaitingForResponseFromServer = false;
                    var data =res.data;
                    if(data.hasOwnProperty('ResponseResult') && data.hasOwnProperty('ResponseData') && (data.ResponseResult==true || data.ResponseResult=='true' || data.ResponseResult==1 || data.ResponseResult=='1')){
                        window.currentUser=data.ResponseData;
                        window.VueApp.setCurrentUser(data.ResponseData);
                        localStorage.setItem("currentUser",JSON.stringify( data.ResponseData));
                        th.loggedIn=true;
                        th.WaitingForResponseFromServer = false;
                        setTimeout(function() { th.redirectToHome(th.rData.to??null); }, 5000);
                    }

                }).catch(function (e) {
                    th.WaitingForResponseFromServer = false;
                    var data = e.response.data.errors;
                    th.errors = data;
                    console.log(data);
                });
            }
        },
        loadingFunction(){
            if(this.words[this.currentLoadidText].length >= this.loading.length){
                this.loading.push(this.words[this.currentLoadidText][this.loading.length-1??0]);

            }else{
                this.currentLoadidText=(this.currentLoadidText==1)?2:1;
                this.loading=[];
            }

        }
    },
    created() {

        setInterval(this.loadingFunction, 50);
        this.loggedIn=(this.alreadyLoggedIn=="" || this.alreadyLoggedIn==" " || this.alreadyLoggedIn=="0" ||this.alreadyLoggedIn=="false" ||this.alreadyLoggedIn==false)?false:true;
       // console.log(this.alreadyLoggedIn);
        var th =this;
        if(this.loggedIn)setTimeout(function() { th.redirectToHome(this.rData.to??null); }, 5000);
    }
        ,
    computed:{



    },

    watch:{
        WaitingForResponseFromServer(n){
            if(n)this.loading=[];
        }
    }


}
    );
