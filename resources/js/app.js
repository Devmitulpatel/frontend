/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Toasted from 'vue-toasted';

require('./bootstrap');

window.Vue = require('vue');


window.backend_source=[ process.env.MIX_APP_BACKEND_DOMAIN_TYPE, [process.env.MIX_APP_BACKEND_DOMAIN,process.env.MIX_APP_BACKEND_DOMAIN_PORT].join(':'),'/' ].join('');






Vue.use(Toasted,{
    theme: "toasted-primary",
    position: "bottom-right",
    duration : 1000,
    iconPack : 'fontawesome'
});

window.VueApp=new Vue({
    el:'#vue-app',
    data(){
        return  {
            currentUser:{},
            currentCart:[],
            currentOrder:{}
        }
    },
    async mounted(){

        //    console.log(JSON.parse(localStorage.getItem('currentUser')));


        var th=this;
        Vue.nextTick(function () {
            th.currentUser= JSON.parse(localStorage.getItem('currentUser'));
            th.getCurrentCart();
        })



    },
    methods:{

        setCurrentOrder(address){
            this.currentOrder.address=address;
            this.currentOrder.cart=this.currentCart;
            localStorage.setItem("currentOrder",JSON.stringify(this.currentOrder));
            this.currentCart=[];
            console.log('clicked');
            this.updateMasterCart(this.currentCart);



        },
        getCurrentCart(){
            var th=this;
            if(this.currentCart.length<1){

                th.currentCart= JSON.parse(localStorage.getItem('currentCart'));
                this.updateMasterCart(th.currentCart);

            }

            return this.currentCart;
        },


        deleteFromCart(p){
            var id=p.id;
            var foundProduct=this.currentCart.findIndex(x=>this.findProductById(x,id));
            if(foundProduct===-1)return false;
            this.currentCart.splice(foundProduct,1);


        },

        remoceQtCart(p){
            var id=p.id;
            var foundProduct=this.currentCart.findIndex(x=>this.findProductById(x,id));
            if(foundProduct===-1)return false;
            if(this.currentCart[foundProduct].qt>1){
                this.currentCart[foundProduct].qt = (this.currentCart[foundProduct].qt > 0) ? this.currentCart[foundProduct].qt - 1 : 0;
                if (this.currentCart[foundProduct].qt == 0) {
                    this.currentCart.splice(foundProduct, 1);
                }
                this.updateAllCart(this.currentCart);
            }

        },





        updateAllCart(c){
            this.currentCart=[];
            this.currentCart=c;


        },
        butitnow(p,u){
            if(!this.productExistInCart(p)) this.addToCart(p);
            window.location.href=u;
        },
        getAProduct(p){
            if(this.currentCart===null)this.currentCart=[];
            if(this.currentCart!=null) {
                var id = p.id;
                var foundProduct = this.currentCart.findIndex(x => this.findProductById(x, id));
              //  console.log(this.currentCart);
                if (foundProduct === -1)   return {qt:0};
                return this.currentCart[foundProduct];
            }else {

                return {qt: 0};
            }
        },

        findProductById(x,id){

            return x.id==id;


        },
        productExistInCart(p){
            var id=p.id;
            var foundProduct=this.currentCart.findIndex(x=>this.findProductById(x,id));
           // console.log(this.currentCart+'2');
            if(foundProduct===-1)return false;
            return true;

        },

        setCurrentUser(u){
            this.currentUser=u;
        },

        updateMasterCart(c,v=false){
            this.currentCart=[];
            this.currentCart=c;
            var carts_1=this.$refs['cart_1'];
            carts_1.updateCartFromOthers(c);
            if(!v)localStorage.setItem("currentCart",JSON.stringify(c));
        },

        addToCart(p){



            var carts_1=this.$refs['cart_1'];
            //   var carts_2=this.$refs['cart_2'];
            carts_1.addFromHome(p)
            this.updateAllCart(this.currentCart);
            //   carts_2.addFromHome(p)


        },
        removeFromCart(key){
            var carts_1=this.$refs['cart_1'];
            //    var carts_2=this.$refs['cart_2'];
            carts_1.removeFrom(key);
            this.updateAllCart(this.currentCart);
            //   carts_2.removeFrom(key);
        },

        clickBtn(url){
            window.location.href=url;
            // $('btn-close-off-canvas')
        }

    },
    computed:{

    }
});
window.addEventListener('storage', () => {
    window.VueApp.updateMasterCart( JSON.parse(localStorage.getItem('currentCart')) ,true  );
});
import './partials/index'
