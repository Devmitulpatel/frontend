@extends('layout.root')


@section('body')

    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Error 404</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Error 404</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="error-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="error_form">
                        <h1>404</h1>
                        <h2>Opps! PAGE NOT BE FOUND</h2>
                        <p>Sorry but the page you are looking for does not exist, have been<br>
                            removed, name changed or is temporarily unavailable.</p>
                        <form action="#">
                            <input placeholder="Search..." type="text">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>

                        @php

                            $url=request()->url();
                           $se=session()->all();
                        @endphp

                        <a href="{{  (  array_key_exists('_previous',$se) && array_key_exists('url',$se['_previous']) && $url!= $se['_previous']['url'] )?$se['_previous']['url'] : url('/')}}">Back to @if(array_key_exists('_previous',$se) && array_key_exists('url',$se['_previous'])  && $url!= $se['_previous']['url']) previous  @else home @endif page</a> </div>
                </div>
            </div>
        </div>
    </div>


@endsection
