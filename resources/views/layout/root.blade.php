<!doctype html>
<html class="no-js" lang="en">
@include('layout.partials.head')

<body>
<div class="home-wrapper home-3" id="vue-app" >
@include('layout.partials.header',['active'=>$active??'home'])

@yield('body')

@include('layout.partials.footer')
</div>

@include('layout.partials.modal')

<!-- Scroll to Top Start -->
<a class="scroll-to-top" href="#"> <i class="ion-chevron-up"></i> </a>
<!-- Scroll to Top End -->


@include('layout.partials.js')

</body>
</html>
