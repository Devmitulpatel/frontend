<!-- Footer Area Start Here -->
<footer class="footer-area" v-cloak>
    <div class="footer-widget-area">
        <div class="container container-default custom-area">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-custom">
                    <div class="single-footer-widget m-0">
                        <div class="footer-logo"> <a href="index.html"> <img style="box-shadow:3px 3px 0 0 rgb(81, 178, 117); max-height:98px;background-color: rgba(255, 255, 255,1);border-radius: 2px;padding: 10px 20px;" src="{{asset('assets/images/logo/bottom.png')}}" alt="Logo Image"> </a> </div>
                        <p class="desc-content">R-Mart is the best parts shop of your daily nutritions. What kind of nutrition do you need you can get here soluta nobis</p>
                        <div class="social-links">
                            <ul class="d-flex">
                                <li> <a class="border rounded-circle" href="#" title="Facebook"> <i class="fa fa-facebook-f"></i> </a> </li>
                                <li> <a class="border rounded-circle" href="#" title="Twitter"> <i class="fa fa-twitter"></i> </a> </li>
                                <li> <a class="border rounded-circle" href="#" title="Linkedin"> <i class="fa fa-linkedin"></i> </a> </li>
                                <li> <a class="border rounded-circle" href="#" title="Youtube"> <i class="fa fa-youtube"></i> </a> </li>
                                <li> <a class="border rounded-circle" href="#" title="Vimeo"> <i class="fa fa-vimeo"></i> </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">Information</h2>
                        <ul class="widget-list">
                            <li><a href="{{route('aboutus')}}">Our Company</a></li>
                            <li><a href="{{route('contactus')}}">Contact Us</a></li>
                            <li><a href="about-us.html">Our Services</a></li>
                            <li><a href="about-us.html">Why We?</a></li>
                            <li><a href="about-us.html">Careers</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">Quicklink</h2>
                        <ul class="widget-list">
                            <li><a href="{{route('aboutus')}}">About</a></li>
                            <li><a href="blog-grid.html" v-if="false">Blog</a></li>
                            <li><a href="{{route('shop')}}">Shop</a></li>
                            <li><a href="{{route('cart')}}">Cart</a></li>
                            <li><a href="{{route('contactus')}}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">Support</h2>
                        <ul class="widget-list">
                            <li><a href="{{route('contactus')}}">Online Support</a></li>
                            <li><a href="contact-us.html">Shipping Policy</a></li>
                            <li><a href="contact-us.html">Return Policy</a></li>
                            <li><a href="contact-us.html">Privacy Policy</a></li>
                            <li><a href="contact-us.html">Terms of Service</a></li>
                        </ul>
                    </div>
                </div>


                @php
                    $settings=App\Models\SettingModel::all()->first()->toArray();

                @endphp


                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">See Information</h2>
                        <div class="widget-body">
                            <address>
                                {!! $settings['contact_us']  !!}
                            </address>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-area" style="position: sticky;bottom: 0px;left: 0px;">
        <div class="container custom-area">
            <div class="row">
                <div class="col-12 text-center col-custom">
                    <div class="copyright-content">
                        <p>Copyright © 2020 <a href="#">R-Mart</a> | Designed by <a target="_blank" href="https://classytechnosoft.com/">Classy Technosoft</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End Here -->
