<!-- Header Area Start Here -->
@php
$active=$active??'home';

@endphp
<header class="main-header-area" style="position: -webkit-sticky;
  position: sticky
  ;top: 0;left: 0;z-index: 99;background-color: rgba(255,255,255,1);box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2)" >
    <!-- Main Header Area Start -->
    <div class="main-header" >
        <div class="container container-default custom-area">
            <div class="row" v-cloak>
                <div class="col-12 col-lg-12 col-custom">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-xl-2 col-sm-6 col-6 col-custom">
                            <div class="header-logo d-flex align-items-center"> <a href="{{route('home')}}"> <img class="img-full" src="{{asset('assets/images/logo/top.png')}}" alt="Header Logo"> </a> </div>
                        </div>
                        <div class="col-lg-8 col-xl-7 position-static d-none d-lg-block col-custom">
                            <nav class="main-nav d-flex justify-content-center">
                                <ul class="nav">
                                    <li> <a class="{{ ($active=='home')?'active':''  }}" href="{{route('home')}}"> <span class="menu-text"> Home</span> </a> </li>
                                    <li> <a class="{{ ($active=='shop')?'active':''  }}" href="{{route('shop')}}"> <span class="menu-text">Shop</span> </a> </li>

                                    <li> <a href="#"> <span class="menu-text"> More</span> <i class="fa fa-angle-down"></i> </a>
                                        <ul class="dropdown-submenu dropdown-hover">
                                            <li><a href="frequently-questions.html">FAQ</a></li>
                                            <li><a href="frequently-questions.html">Blog</a></li>

                                        </ul>
                                    </li>
                                    <li> <a class="{{ ($active=='aboutus')?'active':''  }}" href="{{route('aboutus')}}"> <span class="menu-text">Know About Us</span> </a> </li>
                                    <li> <a class="{{ ($active=='contactus')?'active':''  }}" href="{{route('contactus')}}"> <span class="menu-text">Contact Us</span> </a> </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-lg-2 col-xl-3 col-sm-6 col-6 col-custom">
                            <div class="header-right-area main-nav">
                                <ul class="nav">
                                    <li class="login-register-wrap d-none d-xl-flex">


                                    @if(!auth()->guard('customer')->check())
                                            <span><a href="{{route('login')}}" style="{{ ($active=='signin')?'color:#54b175':''  }}">Login</a></span>

                                            <span><a href="{{route('register')}}" style="{{ ($active=='signup')?'color:#54b175':''  }}">Register</a></span>

                                    @else

                                            <span><a href="{{route('userProfile')}}" style="{{ ($active=='myaccount')?'color:#54b175':''  }}"> My Account</a></span>

                                            <span><a href="{{route('logout')}}">Sign out</a></span>
                                        @endif



                                    </li>

                                    @include('layout.partials.cart' ,['key'=>1,'ref'=>'cart_1','active'=>$active??'home'])

                                    <li class="mobile-menu-btn d-lg-none"> <a class="off-canvas-btn" href="#"> <i class="fa fa-bars"></i> </a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Header Area End -->

    <!-- off-canvas menu start -->
    <aside class="off-canvas-wrapper" id="mobileMenu" >
        <div class="off-canvas-overlay"></div>
        <div class="off-canvas-inner-content">
            <div class="btn-close-off-canvas"> <i class="fa fa-times"></i> </div>
            <div class="off-canvas-inner">
                <div class="search-box-offcanvas">
                    <form>
                        <input type="text" placeholder="Search product...">
                        <button class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- mobile menu start -->
                <div class="mobile-navigation">

                    <!-- mobile menu navigation start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children" v-on:click="clickBtn('{{route('home')}}')"><a href="#">Home</a> </li>
                            <li class="menu-item-has-children" v-on:click="clickBtn('{{route('shop')}}')" ><a href="#">Shop</a> </li>

                            <li class="menu-item-has-children "><a href="#">More</a>
                                <ul class="dropdown">
                                    <li><a href="frequently-questions.html">FAQ</a></li>
                                    <li><a href="frequently-questions.html">Blog</a></li>

                                </ul>
                            </li>
                            <li v-on:click="clickBtn('{{route('aboutus')}}')" ><a href="#">About Us</a></li>
                            <li v-on:click="clickBtn('{{route('contactus')}}')" ><a href="#">Contact</a></li>
                        </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                </div>
                <!-- mobile menu end -->
                <div class="header-top-settings offcanvas-curreny-lang-support">
                    <!-- mobile menu navigation start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children"><a href="#">My Account</a>
                                <ul class="dropdown">

                                    @if(!auth()->guard('customer')->check())
                                    <li v-on:click="clickBtn('{{route('login')}}')" ><a href="#">Login</a></li>
                                    <li v-on:click="clickBtn('{{route('register')}}')" ><a href="#">Register </a></li>

                                    @else
                                        <li v-on:click="clickBtn('{{route('userProfile')}}')" ><a href="#">My Profile</a></li>
                                        <li v-on:click="clickBtn('{{route('logout')}}')" ><a href="#">Sign out</a></li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                </div>
                <!-- offcanvas widget area start -->
                <div class="offcanvas-widget-area">
                    <div class="top-info-wrap text-left text-black">
                        <ul>
                            <li> <i class="fa fa-phone"></i> <a href="info@yourdomain.com">(1245) 2456 012</a> </li>
                            <li> <i class="fa fa-envelope"></i> <a href="info@yourdomain.com">info@yourdomain.com</a> </li>
                        </ul>
                    </div>
                    <div class="off-canvas-widget-social"> <a title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a> <a title="Twitter" href="#"><i class="fa fa-twitter"></i></a> <a title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a> <a title="Youtube" href="#"><i class="fa fa-youtube"></i></a> <a title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a> </div>
                </div>
                <!-- offcanvas widget area end -->
            </div>
        </div>
    </aside>
    <!-- off-canvas menu end -->
</header>
<!-- Header Area End Here -->
