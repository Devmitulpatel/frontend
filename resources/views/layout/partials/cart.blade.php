

<mcart  inline-template ref="{{$ref}}" key="{{$key}}" >



    <li class="minicart-wrap " v-cloak>
        <a href="#" class="minicart-btn toolbar-btn" v-cloak >
            <i class="ion-bag" style="{{ ($active=='cart')?'color:#54b175':''  }}"></i> <span class="cart-item_count" style="{{ ($active=='cart')?'background-color:#54b175':''  }}"> @{{ currentCart.length }}</span>
        </a>
        <div class="cart-item-wrapper dropdown-sidemenu dropdown-hover-2">

            <div class="custom-y-scroll-cart">
                <div v-for="(item,key) in currentCart" class="single-cart-item">

                    <div class="cart-img"> <a :href="item.view_url"><img :src="urlbase+item.pimg" alt=""></a> </div>
                    <div class="cart-text">
                        <h5 class="title"><a :href="item.view_url">@{{key+1}}. @{{item.pname}}</a></h5>
                        <div class="cart-text-btn">
                            <div class="cart-qty"> <span>@{{ item.qt }}×</span> <span class="cart-price">₹ @{{ parseFloat(item.default_price-((item.discount/100)*item.default_price )).toFixed(2) }}</span> </div>
                            <button class="custom-cart-delete-btn" type="button" v-on:click="removeFromCart(key)"><i class="ion-trash-b"></i></button>
                        </div>
                    </div>

                </div>

            </div>



            <div v-if="currentCart.length<1" class="single-cart-item empty-cart">

                <div class="cart-text">
                    <img src="{{asset('imgs/empty.svg')}}">
                    <h5 class="title"><a href="cart.html">Empty Cart</a></h5>

                </div>
            </div>







            <div class="cart-price-total d-flex justify-content-between">
                <h5>Total :</h5>
                <h5> <span class="custom-currency">₹</span> <strong class="custom-amount">@{{parseFloat(totalAmount).toFixed(2)}}</strong> <small v-if="totalAmount>0" class="custom-tax-perfix" v-if="totalAmount>0 || true">+ TAXES</small></h5>
            </div>
            <div class="cart-links d-flex justify-content-center"> <a class="R-Mart-button white-btn" href="{{route('cart')}}">View cart</a> <a class="R-Mart-button white-btn" href="{{route('cart')}}">Checkout</a> </div>
        </div>
    </li>


</mcart>
