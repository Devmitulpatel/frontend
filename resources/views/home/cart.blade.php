@extends('layout.root',['active'=>'cart'])


@section('body')

    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Shopping Cart</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Cart</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->

    <mcart-detailed inline-template  v-bind:cart="currentCart">
        <div>
    <!-- cart main wrapper start -->
    <div class="cart-main-wrapper mt-no-text mb-no-text mt-0">
        <div class="container mt-5">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-xl-9 p-0" :class="{ 'col-xl-12 col-lg-12':currentCart.length < 1 }">
                    <div class="col">

                        <!-- Cart Table Area -->
                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="pro-thumbnail">Image</th>
                                    <th class="pro-title">Product</th>
                                    <th class="pro-price">Price</th>
                                    <th class="pro-quantity">Quantity</th>
                                    <th class="pro-subtotal">Total</th>
                                    <th class="pro-remove">Remove</th>
                                </tr>
                                </thead>



                                <tbody>
                                <tr v-for="c in currentCart" v-cloak>

                                    <td class="pro-thumbnail"><a href="#"><img class="img-fluid" :src="[c.base_url,c.pimg].join('')" alt="Product" /></a></td>
                                    <td class="pro-title"><a href="#">@{{ c.pname }} <br>
                                            @{{ c.default_unit }}</a></td>
                                    <td class="pro-price"><span>₹ @{{ parseFloat(c.default_price - ((c.discount/100)* c.default_price)).toFixed(2)  }}</span></td>
                                    <td class="pro-quantity">
                                        <div class="quantity">




                                            <div class="btn-group " role="group" aria-label="First group">
                                                <button type="button" v-on:click="addToCart(c)"  class="btn cart-qt-btn-plus"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-qt">@{{getAProduct(c).qt}}</button>
                                                <button type="button" v-on:click="remoceQtCart(c)" class="btn cart-qt-btn-minus"><i class="fa fa-minus"></i></button>
                                            </div>



                                        </div>
                                    </td>
                                    <td class="pro-subtotal"><span>₹ @{{ parseFloat(c.qt*(c.default_price - ((c.discount/100)* c.default_price))).toFixed(2) }}</span></td>
                                    <td class="pro-remove"><a v-on:click="deleteFromCart(c)"><i class="ion-trash-b"></i></a></td>
                                </tr>

                                <tr v-if="currentCart.length < 1" v-cloak>
                                    <td colspan="6">

                                        <div class="row">
                                            <h6 class="empty-cart-text col-12 text-center" ><a href="cart.html">Empty Cart</a> <img src="{{asset('imgs/empty.svg')}}" ></h6>

                                        </div>

                                    </td>

                                </tr>

                                </tbody>


                            </table>
                        </div>
                        <!-- Cart Update Option -->
                        <div class="cart-update-option d-block d-md-flex justify-content-between" v-if="currentCart.length > 0">
                            <div class="apply-coupon-wrapper" >
                                <form action="#" method="post" class=" d-block d-md-flex">
                                    <input type="text" placeholder="Enter Your Coupon Code" required />
                                    <button class="btn R-Mart-button primary-btn">Apply Coupon</button>
                                </form>
                            </div>
                            <div class="cart-update mt-sm-16" v-if="false"> <a href="#" class="btn R-Mart-button primary-btn">Update Cart</a> </div>
                        </div>


                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 p-0"  v-if="currentCart.length > 0">
                    <div class="col ml-auto ">
                        <!-- Cart Calculation Area -->
                        <div class="cart-calculator-wrapper ">
                            <div class="cart-calculate-items cart-subtotal-div">
                                <h3>Shopping Cart</h3>
                                <div class="table-responsive subtotal-div">
                                    <table class="table" v-cloak >
                                        <tr>
                                            <td>Sub Total</td>
                                            <td>₹ @{{ parseFloat(totalAmount).toFixed(2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping</td>
                                            <td>₹ @{{ (this.currentCart.length>0)? parseFloat(shipping).toFixed(2) :parseFloat(0).toFixed(2) }}</td>
                                        </tr> <tr>
                                            <td>Tax <small>(@{{ taxrate }}%)</small></td>
                                            <td>₹ @{{ parseFloat((taxrate /100)*totalAmount).toFixed(2) }}</td>
                                        </tr>
                                        <tr class="total">
                                            <td>Total</td>
                                            <td class="total-amount">₹ @{{ parseFloat(  totalAmount + ( (taxrate /100)*totalAmount )  + ((this.currentCart.length>0)?shipping:0)  ).toFixed(2) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            @if(!auth()->guard('customer')->check())
                                <a href="{{route('login',['to'=>route('checkout')])}}" class="btn R-Mart-button primary-btn d-block cart-subtotal-checkout">Sign & Checkout</a> </div>
                            @else
                                <a href="{{route('checkout')}}" class="btn R-Mart-button primary-btn d-block cart-subtotal-checkout">Proceed To Checkout</a> </div>
                            @endif


                    </div>
                </div>


            </div>

        </div>
    </div>
    <!-- cart main wrapper end -->

        </div>

    </mcart-detailed>
@endsection
