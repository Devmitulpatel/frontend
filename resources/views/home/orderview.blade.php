@extends('layout.root',['active'=>'aboutus'])


@section('body')

    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Order No.  {{ $order['oid']}}</h3>
                        <ul>
                            <li><a  href="{{route('userProfile')}}">My Account</a></li>
                            <li><a href="{{route('userProfile',['activeTab'=>'orders'])}}">Orders</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->





      <div>
    <!-- cart main wrapper start -->
    <div class="cart-main-wrapper mt-no-text mb-no-text mt-0">
        <div class="container mt-5">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-xl-9 p-0" >
                    <div class="col">

                        <!-- Cart Table Area -->
                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="pro-thumbnail">Image</th>
                                    <th class="pro-title">Product</th>
                                    <th class="pro-price">Price</th>
                                    <th class="pro-quantity">Quantity</th>
                                    <th class="pro-subtotal">Total</th>

                                </tr>
                                </thead>



                                <tbody>

                                @foreach($order['products'] as $p)

                                <tr>



                                    <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="{{$p['more']['base_url'].$p['more']['pimg']}}" alt="Product" /></a>

                                    </td>
                                    <td class="pro-title"><a href="#">
                                            {{ $p['name'] }}
                                            </a>

                                        <small class="text-muted"><br>
                                            {{ $p['type'] }}</small>
                                    </td>
                                    <td class="pro-price"><span>₹ {{ $p['price'] }}</span></td>
                                    <td class="pro-quantity">
                                        <div class="quantity">
                                            {{ $p['pqt'] }}

                                        </div>
                                    </td>
                                    <td class="pro-subtotal"><span>₹ {{number_format($p['price']*$p['pqt'],2)}} </span></td>

                                </tr>
                                @endforeach

                                <tr v-if="false">
                                    <td colspan="6">

                                        <div class="row">
                                            <h6 class="empty-cart-text col-12 text-center" ><a href="cart.html">Empty Cart</a> <img src="{{asset('imgs/empty.svg')}}" ></h6>

                                        </div>

                                    </td>

                                </tr>

                                </tbody>


                            </table>
                        </div>


                    <div class="row">
                        <div class="col ml-auto " >
                            <!-- Cart Calculation Area -->
                            <div class="cart-calculator-wrapper " >
                                <div class="cart-calculate-items cart-subtotal-div" style="border-radius: 0">
                                    <h3>Address Details</h3>
                                    <div class="table-responsive subtotal-div">
                                        <table class="table" >

                                            <tr class="select-none">
                                                <td>Plot/Block No.</td>
                                                <td> <small class=" font-weight-bold  px-2 py-1">{{ $order['address']['hno']}}</small></td>
                                            </tr>  <tr class="select-none">
                                                <td>Area/Street</td>
                                                <td> <small class=" px- font-weight-bold 2 py-1  ">{{ $order['address']['society'] }}</small></td>
                                            </tr>
                                            <tr class="select-none">
                                                <td>Payment Method</td>
                                                <td> <small class=" px-2 py-1 font-bold text-muted" style="font-size: 75%">{{ $order['p_method'] }}</small></td>
                                            </tr>
                                            <tr class="select-none">
                                                <td>Status</td>

                                                @switch ($order['status'])


                                                    @case('completed')
                                                    <td class="text-success"><small class="border border-success px-2 py-1">Delivered</small></td>
                                                    @break

                                                    @case('cancelled')
                                                    <td class="text-danger"><small class="border border-danger px-2 py-1">Cancelled</small></td>
                                                    @break

                                                    @case('pending')
                                                    <td class="text-info"><small class="border border-info px-2 py-1">in Process</small></td>
                                                    @break

                                                @endswitch



                                            </tr>
                                            <tr>
                                                <td>Sub Total</td>
                                                <td>₹ {{  number_format($order['ptotal'],2)  }}</td>
                                            </tr>
                                            <tr>
                                                <td>Shipping</td>
                                                <td>₹ {{  ( $order['total'] - ($order['ptotal']+( $order['total'] * ($order['tax']/100) ) )   >10 ) ?$order['total'] - ( $order['ptotal']+( $order['total'] * ($order['tax']/100) ))        :0 }} </td>
                                            </tr> <tr>
                                                <td>Tax <small>( {{ $order['tax']}}% )</small></td>
                                                <td>₹ {{  number_format($order['total'] * ($order['tax']/100),2)  }}</td>
                                            </tr>
                                            <tr class="total">
                                                <td>Total</td>
                                                <td class="total-amount">₹ {{  number_format($order['total'],2)  }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>



                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 p-0" >
                    <div class="col ml-auto ">
                        <!-- Cart Calculation Area -->
                        <div class="cart-calculator-wrapper ">
                            <div class="cart-calculate-items cart-subtotal-div">
                                <h3>Order Details</h3>
                                <div class="table-responsive subtotal-div">
                                    <table class="table" >

                                        <tr class="select-none">
                                            <td>Order No</td>
                                            <td> <code class=" font-weight-bold  px-2 py-1">{{ $order['oid']}}</code></td>
                                        </tr>  <tr class="select-none">
                                            <td>Date</td>
                                            <td> <small class=" px-2 py-1 font-bold ">{{ \Carbon\Carbon::parse($order['order_date'])->toFormattedDateString() }}</small></td>
                                        </tr>
                                        <tr class="select-none">
                                            <td>Payment Method</td>
                                            <td> <small class=" px-2 py-1 font-bold text-muted" style="font-size: 75%">{{ $order['p_method'] }}</small></td>
                                        </tr>
                                        <tr class="select-none">
                                            <td>Status</td>

                                                @switch ($order['status'])


                                                @case('completed')
                                                <td class="text-success"><small class="border border-success px-2 py-1">Delivered</small></td>
                                                @break

                                                @case('cancelled')
                                                <td class="text-danger"><small class="border border-danger px-2 py-1">Cancelled</small></td>
                                                @break

                                                @case('pending')
                                                <td class="text-info"><small class="border border-info px-2 py-1">in Process</small></td>
                                                @break

                                                @endswitch



                                        </tr>
                                        <tr>
                                            <td>Sub Total</td>
                                            <td>₹ {{  number_format($order['ptotal'],2)  }}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping</td>
                                            <td>₹ {{  ( $order['total'] - ($order['ptotal']+( $order['total'] * ($order['tax']/100) ) )   >10 ) ?$order['total'] - ( $order['ptotal']+( $order['total'] * ($order['tax']/100) ))        :0 }} </td>
                                        </tr> <tr>
                                            <td>Tax <small>( {{ $order['tax']}}% )</small></td>
                                            <td>₹ {{  number_format($order['total'] * ($order['tax']/100),2)  }}</td>
                                        </tr>
                                        <tr class="total">
                                            <td>Total</td>
                                            <td class="total-amount">₹ {{  number_format($order['total'],2)  }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                            @if ($order['status']=='pending')
                                <cancel-order-btn inline-template  url="{{route('orderCancel',$order['id'])}}">
                                    <div class="btn btn-block btn-lg btn-outline-danger py-1 mb-2 bg-danger text-black-50" style="border-radius: 0" v-cloak v-on:click="performAction()">
                                        <i class="fa fa-close mr-2 uppercase " ></i> @{{ "Cancel".toUpperCase() }}
                                    </div>
                                </cancel-order-btn>
                            @endif

                            @if ($order['status']=='cancelled' || $order['status']=='completed')
                                <div class="bg-info  btn btn-block btn-lg btn-outline-danger py-1 mb-2 text-black-50" style="border-radius: 0" v-cloak>
                                    <i class="fa fa-repeat"></i> @{{ "Repeat Order".toUpperCase() }}


                                </div>

                            @endif

                            <a href="{{route('userProfile',['activeTab'=>'orders'])}}" class="btn R-Mart-button primary-btn d-block cart-subtotal-checkout">
                                <i class="fa fa-backward mr-2" ></i>Back To orders</a> </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <!-- cart main wrapper end -->

        </div>


@endsection
