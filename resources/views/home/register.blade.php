@extends('layout.root',['active'=>'signup'])


@section('body')


    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Login-Register</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Login-Register</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->
    <!-- Login Area Start Here -->
    <div class="login-register-area mt-no-text mb-no-text">
        <div class="container container-default-2 custom-area">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-custom">
                    <div class="login-register-wrapper">
                        <div class="section-content text-center mb-5">
                            <h2 class="title-4 mb-2">Create Account</h2>
                            <p class="desc-content">Please Register using account detail bellow.</p>
                        </div>

                        <register-form  inline-template>

                        <div>

                            <div v-if="!WaitingForResponseFromServer">
                            <div class="single-input-item mb-3" :class="{ 'input-has-error':errors.hasOwnProperty('fname')}">
                                <input v-model="form.fname" type="text" placeholder="First Name" >
                                <small v-if="errors.hasOwnProperty('fname')" v-cloak>

                                    <div class="alert alert-danger" role="alert">
                                        <span v-for="er in errors.fname"> @{{ er }}</span>
                                    </div>

                                </small>
                            </div>
                            <div class="single-input-item mb-3" :class="{ 'input-has-error':errors.hasOwnProperty('lname')}">
                                <input v-model="form.lname" type="text" placeholder="Last Name">
                                <small v-if="errors.hasOwnProperty('lname')" v-cloak>

                                    <div class="alert alert-danger" role="alert">
                                        <span v-for="er in errors.lname"> @{{ er }}</span>
                                    </div>

                                </small>
                            </div>
                            <div class="single-input-item mb-3" :class="{ 'input-has-error':errors.hasOwnProperty('email')}">
                                <input v-model="form.email" type="email" placeholder="Enter Email">
                                <small v-if="errors.hasOwnProperty('email')" v-cloak>

                                    <div class="alert alert-danger" role="alert">
                                        <span v-for="er in errors.email"> @{{ er }}</span>
                                    </div>

                                </small>
                            </div>
                            <div class="single-input-item mb-3" :class="{ 'input-has-error':errors.hasOwnProperty('password')}">
                                <input v-model="form.password" type="password" placeholder="Enter your Password">
                                <small  v-if="errors.hasOwnProperty('password')" v-cloak>

                                    <div class="alert alert-danger" role="alert">
                                        <span v-for="er in errors.password"> @{{ er }}</span>
                                    </div>

                                </small>
                            </div>
                            <div class="single-input-item mb-3" :class="{ 'input-has-error':errors.hasOwnProperty('password_confirmation')}">
                                <input v-model="form.password_confirmation" type="password" placeholder="Enter your Password Again">
                                <small v-if="errors.hasOwnProperty('password_confirmation')" v-cloak>

                                    <div class="alert alert-danger" role="alert">
                                        <span v-for="er in errors.password_confirmation"> @{{ er }}</span>
                                    </div>

                                </small>
                            </div>

                            <div v-if="false" class="single-input-item mb-3">
                                <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                    <div class="remember-meta mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="rememberMe">
                                            <label class="custom-control-label" for="rememberMe">Subscribe Our Newsletter</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>

                            <div v-else class="loading-box"  v-cloak>
                                <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                <h6><span v-for="i in loading">@{{ i }}</span> </h6>
                            </div>

                            <div class="single-input-item mb-3">
                                <button v-on:click="loginPress('{{route('register')}}')" class="btn R-Mart-button-2 primary-color">Register</button>
                            </div>
                        </div>
                        </register-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Login Area End Here -->
@endsection
