@extends('layout.root',['active'=>'myaccount'])


@section('body')



    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Welcome {{$user['name']}}</h3>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>My Account</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->
    <!-- my account wrapper start -->
    <div class="my-account-wrapper mt-no-text mb-no-text">
        <div class="container container-default-2 custom-area">
            <div class="row">
                <div class="col-lg-12 col-custom">
                    <!-- My Account Page Start -->
                    <div class="myaccount-page-wrapper">
                        <!-- My Account Tab Menu Start -->
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-custom">
                                <div class="myaccount-tab-menu nav" role="tablist">
                                    <a href="#dashboad" @if(!array_key_exists('activeTab',$data)) class="active" @endif data-toggle="tab"><i class="fa fa-dashboard"></i> Dashboard</a>
                                    <a href="#orders"  @if(array_key_exists('activeTab',$data)  && $data['activeTab']=='orders') class="active" @endif data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>
                                    <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Payment Method</a>
                                    <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> address</a>
                                    <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> Account Details</a>
                                    <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a>
                                </div>
                            </div>
                            <!-- My Account Tab Menu End -->

                            <!-- My Account Tab Content Start -->
                            <div class="col-lg-9 col-md-8 col-custom">
                                <div class="tab-content" id="myaccountContent">
                                    <!-- Single Tab Content Start -->
                                    <div class="tab-pane fade @if(!array_key_exists('activeTab',$data)) show active @endif" id="dashboad" role="tabpanel">
                                        <div class="myaccount-content">
                                            <h3>Dashboard</h3>
                                            <div class="welcome">
                                                <p>Hello, <strong>{{$user['name']}}</strong> ( If Not <strong>{{$user['name']}} ! </strong><a href="{{route('logout')}}" class="logout text-danger"> Logout</a> )</p>
                                            </div>
                                            <p class="mb-0">From your account dashboard. you can easily check & view your recent orders, manage your shipping and billing addresses and edit your password and account details.</p>
                                        </div>
                                    </div>
                                    <!-- Single Tab Content End -->

                                    <!-- Single Tab Content Start -->
                                    @php
                                    $model=\App\Models\OrdersModel::where(['uid'=>$user['id']])->get();



                                    $orders=($model->count()>0)?$model->toArray():[];

                                    @endphp


                                    <div class="tab-pane fade @if(array_key_exists('activeTab',$data)  && $data['activeTab']=='orders') show active @endif" id="orders" role="tabpanel">
                                        <div class="myaccount-content">
                                            <h3>Orders</h3>
                                            <div class="myaccount-table table-responsive text-center">


                                                <order-list inline-template :url="{{ collect([ 'viewOrder'=>route('orderDetails') ])->toJson()  }}">


                                                <table class="table table-bordered">
                                                    <thead class="thead-light">
                                                    <tr>
                                                        <th>Order</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @foreach($orders as $o)
                                                    <tr>
                                                        <td> <code>{{$o['oid']}}</code></td>
                                                        <td>
                                                            @php
                                                                $date=$o['order_date'];
                                                                $explo=explode('-',$date);

                                                                $dt = Illuminate\Support\Carbon::now();
                                                                $dt->year=$explo[0];
                                                                $dt->month=$explo[1];
                                                                $dt->day=$explo[2];
                                                                $date=$dt->toFormattedDateString();

                                                            @endphp
                                                            {{$date}}
                                                        </td>
                                                        <td>

                                                            @switch($o['status'])

                                                                @case('pending')
                                                                <span class="text-info badge border badge border p-2">Processing</span>
                                                                @break

                                                                @case('completed')
                                                                <span class="text-success badge badge border p-2">Delivered</span>
                                                                @break

                                                                @case('cancelled')
                                                                        <span class="text-danger badge border p-2">Cancelled</span>
                                                                @break

                                                                @default
                                                                <span class="text-warning badge badge border p-2">{{strtoupper($o['status'])}}</span>

                                                            @endswitch

                                                        </td>
                                                        <td>₹ {{ number_format($o['total'],2) }}</td>
                                                        <td><a  v-on:click="toggleModelOrderView({{ collect($o)->toJson()  }})" href="#" class="btn R-Mart-button-2 primary-color rounded-0">View</a></td>
                                                    </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                                </order-list>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Tab Content End -->

                                    <!-- Single Tab Content Start -->
                                    <div style="display: none" class="tab-pane fade" id="download" role="tabpanel">
                                        <div class="myaccount-content">
                                            <h3>Downloads</h3>
                                            <div class="myaccount-table table-responsive text-center">
                                                <table class="table table-bordered">
                                                    <thead class="thead-light">
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Date</th>
                                                        <th>Expire</th>
                                                        <th>Download</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Haven - Free Real Estate PSD Template</td>
                                                        <td>Aug 22, 2018</td>
                                                        <td>Yes</td>
                                                        <td><a href="#" class="btn R-Mart-button-2 primary-color rounded-0"><i class="fa fa-cloud-download mr-2"></i>Download File</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>HasTech - Profolio Business Template</td>
                                                        <td>Sep 12, 2018</td>
                                                        <td>Never</td>
                                                        <td><a href="#" class="btn R-Mart-button-2 primary-color rounded-0"><i class="fa fa-cloud-download mr-2"></i>Download File</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Tab Content End -->

                                    <!-- Single Tab Content Start -->
                                    <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                        <div class="myaccount-content">
                                            <h3>Payment Method</h3>
                                            <p class="saved-message text-info">Sorry,You have not saved any Payment Method yet.</p>

                                        </div>
                                        <a v-if="true" href="#" class="btn disabled R-Mart-button-2 primary-color rounded-0"><i class="fa fa-edit mr-2"></i>Payment Methods can be edited only from Our Mobile App</a>
                                    </div>
                                    <!-- Single Tab Content End -->

                                    <!-- Single Tab Content Start -->
                                    <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                        <div class="myaccount-content">
                                            <h3>Billing Address</h3>


                                            @foreach($user['address'] as $a)
                                            <address>
                                                <strong>( {{$loop->iteration}} ) {{strtoupper($a['name']) }}</strong>
                                                <p style="padding-left: 32px">     <small class="text-muted">  {{ strtoupper($a['type']) }} </small></p>
                                                <p style="padding-left: 32px">{{$a['hno']}} , {{$a["society"]}} <br>
                                                    {{$a['landmark']}}, <br>
                                                    {{$a['area']}},{{$a['pincode']}}</p>
                                                <p style="padding-left: 32px">Mobile:{{$user['ccode']}} {{$user['mobile']}}</p>
                                            </address>
                                                <a v-if="false" href="#" class="btn R-Mart-button-2 primary-color rounded-0"><i class="fa fa-edit mr-2"></i>Edit Address ({{strtoupper($a['name']) }})</a>
                                                <a v-if="true" href="#" class="btn disabled R-Mart-button-2 primary-color rounded-0"><i class="fa fa-edit mr-2"></i>Address can be edited only from Our Mobile App</a>

                                            @endforeach
                                        </div>

                                    </div>
                                    <!-- Single Tab Content End -->

                                    <!-- Single Tab Content Start -->
                                    <div class="tab-pane fade" id="account-info" role="tabpanel">


                                        @php
                                            $nameExplode=explode(' ',$user['name']);


                                            $fname=reset($nameExplode);
                                            $lname=(count($nameExplode)>1)?$nameExplode[1]:'';
                                            if(count($nameExplode)>2){
                                                array_shift($nameExplode);
                                                $lname=implode(' ',$nameExplode);
                                          //  dd($nameExplode);
                                            }

                                        $vueUser=[
                                            'fname'=>$fname,
                                            'lname'=>$lname ,
                                            'email'=>$user['email'],
                                            'mobile'=>$user['mobile']
                                        ];
                                        @endphp

                                        <profile-edit :m-data="{{collect($vueUser)->toJson()}}" url="{{route('userProfile')}}" logout="{{ route('logout')  }}" inline-template>
                                        <div class="myaccount-content">
                                            <h3>Account Details</h3>



                                            <div class="account-details-form">
                                                <div action="#">
                                                    <fieldset v-if="!toPassword">
                                                        <legend>Basic Details</legend>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-custom">
                                                            <div class="single-input-item mb-3">
                                                                <label for="first-name" class="required mb-1">First Name</label>
                                                                <input type="text" id="first-name" class="form-control" v-model="form.fname" placeholder="First Name" />
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-custom">
                                                            <div class="single-input-item mb-3">
                                                                <label for="last-name" class="required mb-1">Last Name</label>
                                                                <input type="text" id="last-name" class="form-control" v-model="form.lname"  placeholder="Last Name" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-input-item mb-3">
                                                        <label for="display-name" class="required mb-1">Contact No</label>
                                                        <input type="text" id="display-name" class="form-control" v-model="form.mobile"  placeholder="Display Name" />
                                                    </div>
                                                    <div class="single-input-item mb-3">
                                                        <label for="email" class="required mb-1">Email Addres</label>
                                                        <input type="email" id="email"class="form-control"  v-model="form.email"  placeholder="Email Address" />
                                                    </div>
                                                    </fieldset>

                                                    <fieldset v-if="toPassword">
                                                        <legend>Password change</legend>
                                                        <div class="single-input-item mb-3">
                                                            <label for="current-pwd" class="required mb-1 ">Current Password</label>
                                                            <input type="password" id="current-pwd" v-model="form.current_password" class="form-control" placeholder="Current Password" />
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-custom">
                                                                <div class="single-input-item mb-3">
                                                                    <label for="new-pwd" class="required mb-1 ">New Password</label>
                                                                    <input type="password" id="new-pwd" v-model="form.password" class="form-control" placeholder="New Password" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-custom">
                                                                <div class="single-input-item mb-3">
                                                                    <label for="confirm-pwd" class="required mb-1 ">Confirm Password</label>
                                                                    <input type="password" id="confirm-pwd" v-model="form.password_confirmation" class="form-control" placeholder="Confirm Password" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <div class="btn-group">
                                                        <button class="btn R-Mart-button primary-btn" v-on:click="saveData">Save Changes</button>
                                                        <button class="btn R-Mart-button primary-btn btn-change-psw" v-if="!toPassword"  v-on:click="changePasswordClick1">Change Password</button>
                                                        <button class="btn R-Mart-button primary-btn btn-change-psw" v-if="toPassword"  v-on:click="backToProfileEdit">Change Profile</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        </profile-edit>
                                    </div>
                                    <!-- Single Tab Content End -->
                                </div>
                            </div>
                            <!-- My Account Tab Content End -->
                        </div>
                    </div>
                    <!-- My Account Page End -->
                </div>
            </div>
        </div>

    </div>
    <!-- my account wrapper end -->

@endsection
