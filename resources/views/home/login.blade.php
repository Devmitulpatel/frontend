@extends('layout.root',['active'=>'signin'])


@section('body')

    <!-- Breadcrumb Area Start Here -->
    <div v-if="true" class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Login-Register</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Login-Register</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->
    <!-- Login Area Start Here -->
    <div class="login-register-area mt-no-text mb-no-text">
        <div class="container container-default-2 custom-area">
            <div class="row">


                <div class="col-xs-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" v-cloak >

                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 p-0 ">
                        <div class="login-register-wrapper">
                            <div class="section-content text-center mb-5">
                                <h2 class="title-4 mb-2">Login</h2>
                                <p class="desc-content">Please login using account detail bellow.</p>
                            </div>

                            <login-form :already-logged-in="'{{ (bool)auth()->guard('customer')->check()}}'" inline-template :r-data="{{collect($rData)->toJson()}}" >
                                <div>
                                    <div v-if="!WaitingForResponseFromServer && !loggedIn">
                                        <div class="single-input-item mb-3">
                                            <input :class="{ 'waiting-input': WaitingForResponseFromServer}" :disabled="WaitingForResponseFromServer" :readonly="WaitingForResponseFromServer" type="text" v-model="form.username" placeholder="Email or Username">

                                            <small class="input-has-error" v-if="errors.hasOwnProperty('username')">

                                                <div class="alert alert-danger" role="alert">
                                                   <span v-for="er in errors.username"> @{{ er }}</span>


                                                </div>

                                            </small>

                                        </div>
                                        <div class="single-input-item mb-3">
                                            <input class="{ 'waiting-input': WaitingForResponseFromServer}" :disabled="WaitingForResponseFromServer" :readonly="WaitingForResponseFromServer" type="password"  v-model="form.password" placeholder="Enter your Password">
                                            <small class="input-has-error" v-if="errors.hasOwnProperty('password')">

                                                <div class="alert alert-danger" role="alert">
                                                    <span v-for="er in errors.password"> @{{ er }}</span>
                                                </div>

                                            </small>

                                        </div>
                                        <div class="single-input-item mb-3">
                                            <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                                <div class="remember-meta mb-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" v-model="form.remember" class="custom-control-input" id="rememberMe">
                                                        <label class="custom-control-label" for="rememberMe">Remember Me</label>
                                                    </div>
                                                </div>
                                                <a href="#" class="forget-pwd mb-3">Forget Password?</a> </div>
                                        </div>
                                    </div>

                                    <div v-if="WaitingForResponseFromServer && !loggedIn" class="loading-box">
                                        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                        <h6><span v-for="i in loading">@{{ i }}</span> </h6>
                                    </div>

                                    <div v-if="!WaitingForResponseFromServer && loggedIn">
                                               <img src="{{asset('imgs/loggedin.jpg')}}">
                                        <h5 class="text-center mt-5"> You are Signed in Successfully.<br>You will redirected to Home in 5 Seconds. </h5>

                                    </div>

                                    <div class="single-input-item mb-3"  v-if="!WaitingForResponseFromServer && !loggedIn">
                                        <button v-on:click="loginPress('{{route('login')}}')" class="btn R-Mart-button-2 primary-color btn-block" >Login</button>
                                    </div>


                                </div>

                            </login-form>
                        </div>
                    </div>
                    @if (!auth()->guard('customer')->check())
                        <div class="col-lg-6 offset-lg-3  col-md-8 offset-md-2  p-0 login-side" >
                            <div class="login-register-wrapper">
                                <div class="section-content text-center mb-5">
                                    <h2 class="title-4 mb-2">Dont have account ?</h2>
                                    <p class="desc-content">Get a free account and join our digital community.</p>
                                </div>

                                <div>
                                    <div class="btn btn-block btn-register">Register Here</div>
                                </div>


                            </div>
                        </div>
                    @endif


                </div>



            </div>
        </div>
    </div>
    <!-- Login Area End Here -->

@endsection
