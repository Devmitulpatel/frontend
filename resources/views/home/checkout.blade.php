@extends('layout.root',['active'=>'cart'])


@section('body')

    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Check out</h3>
                        <ul>
                            <li><a href="{{route('home')}}">Home</a></li>
                            <li href="{{route('cart')}}">Cart</li>
                            <li>Select Address</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->

    <select-address inline-template  :address="{{ collect($address)->toJson()  }}" url="{{route('checkoutPayment')}}">
        <div>
    <!-- cart main wrapper start -->
    <div class="cart-main-wrapper mt-no-text mb-no-text mt-0">


        <div class="container mt-3">

            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-12 col-md-6  col-lg-6 ">
                    <div class="input-group ">

                        <div class="input-group-prepend"> <span class="input-group-text">   <i  class="fa fa-map px-2"></i> Select Address </span></div>

                        <select class="form-control" v-model="selctedAddress">
                            <option v-for="a in address" :value="a" >@{{ a.name }}</option>
                        </select>




                    </div>
                </div>
            </div>


        <div class="row justify-content-center" v-if="selctedAddress!==null">
            <div class="col-xs-12 col-sm-12 col-md-6  col-lg-6 ">
                <div class="card text-center  mt-3"  >

                    <div class="card-header ">

                        @{{ selctedAddress.name }}
                    </div>

                    <div class="card-block">
                        <address class=" p-2 mt-2 text-dark">

                            <div class="r">

            <span> @{{
                [
                selctedAddress.hno,
                selctedAddress.society,

                ].join(', ')
                }},
              </span>
                                <br>
                                <span>
                @{{
                [

                selctedAddress.landmark,

                ].join(',')
                }},
                </span>
                                <br>
                                <span>
                @{{

                [ selctedAddress.area.toUpperCase(),selctedAddress.pincode].join(' - ')

                }}.
                    </span>

                            </div>

                        </address>
                    </div>

                    <div class="card-footer">

                        <div class="btn R-Mart-button white-btn" v-on:click="addressSave()">
                            <i  class="fa fa-map-marker px-2"></i>
                            <small>  Deliver on this Address</small> </div>


                    </div>
                </div>
            </div>

        </div>







        </div>





    </div>
    <!-- cart main wrapper end -->

        </div>
    </select-address>
@endsection
