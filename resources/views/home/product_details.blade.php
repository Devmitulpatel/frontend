@extends('layout.root',['active'=>'product'])


@section('body')

    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Product Details</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->
    <!-- Single Product Main Area Start -->
    <div class="single-product-main-area">
        <div class="container container-default custom-area">





            <div class="row">
                <div class="col-lg-5 col-custom">
                    <div class="product-details-img horizontal-tab"  v-cloak>
                        <div class="product-slider popup-gallery product-details_slider" data-slick-options='{
                        "slidesToShow": 1,
                        "arrows": false,
                        "fade": true,
                        "draggable": false,
                        "swipe": false,
                        "asNavFor": ".pd-slider-nav"
                        }'>
                            @php
                            //    dd($product);
                                $base=$product['base_url'];
                            @endphp

                            <div class="single-image border"> <a href="{{implode('/',[$base,$product['pimg']])}}"> <img src="{{implode('/',[$base,$product['pimg']])}}" alt="Product"> </a> </div>

                            @foreach( $product['pimgs'] as $p)
                            <div class="single-image border"> <a href="{{implode('/',[$base,$p])}}"> <img src="{{implode('/',[$base,$p])}}" alt="Product"> </a> </div>
                            @endforeach

                        </div>
                        <div class="pd-slider-nav product-slider" data-slick-options='{
                        "slidesToShow": 4,
                        "asNavFor": ".product-details_slider",
                        "focusOnSelect": true,
                        "arrows" : false,
                        "spaceBetween": 30,
                        "vertical" : false
                        }' data-slick-responsive='[
                            {"breakpoint":1501, "settings": {"slidesToShow": 4}},
                            {"breakpoint":1200, "settings": {"slidesToShow": 4}},
                            {"breakpoint":992, "settings": {"slidesToShow": 4}},
                            {"breakpoint":575, "settings": {"slidesToShow": 3}}
                        ]'>
                            <div class="single-thumb border"> <img src="{{implode('/',[$base,$product['pimg']])}}" alt="Product Thumnail"> </div>
                            @foreach( $product['pimgs'] as $p)
                            <div class="single-thumb border"> <img src="{{implode('/',[$base,$p])}}" alt="Product Thumnail"> </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-custom border py-4">


                    <div class="product-summery position-relative">
                        <div class="product-head mb-3">
                            <h2 class="product-title"> {{$product['pname']}}



                            </h2>
                            @if($product['popular']!=0||$product['popular']!='0'||$product['popular']!='false')<div class="badge badge-info popular-product">
                                <i class="fa fa-bookmark mr-1" aria-hidden="true"></i>Popular Product</div>@endif

                            @if($product['discount']>0)<div class="badge badge-warning discounted-product"  >
                                <i class="fa fa-hashtag mr-1" aria-hidden="true"></i>{{$product['discount']}} % Discount</div>@endif



                        </div>
                        <div class="price-box mb-2"> <span class="regular-price">₹ {{ number_format( $product['default_price'] - ( ($product['discount']/100) * $product['default_price']  ) ,2) }}</span> <span class="old-price text-danger"><del>₹ {{number_format($product['default_price'],2)}}</del></span> </div>
                        <div class="product-rating mb-3"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="sku mb-3">

                                @if($product['stock']==1)
                                Status:    <span class="text-success">in stock</span>
                                @endif

                                @if($product['stock']==0)
                                Status:    <span class="text-danger">out of stock</span>
                                @endif


                                 </div>
                        <p class="desc-content mb-5">

                            @php
                            //dd(substr($product['psdesc'], 0, 100));
                            $limit=5;
                            @endphp

                            {!!  implode(' ',[(strlen($product['psdesc'])>$limit)?substr($product['psdesc'], 0, $limit):$product['psdesc'],(strlen($product['psdesc'])>$limit)?'... <a class="view-more-btn" goto="#product-details-more" onclick="this.scrollIntoView(this.goto)">View More </a>':''])   !!}


                        </p>
                        <div class="quantity-with_btn mb-4  row">
                            <div class="quantity-custom  col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">

                                    <div class="input-group" v-cloak>
                                        <div class="input-group-prepend" >
                                            <span v-on:click="addToCart({{ collect($product)->toJson() }})" class="input-group-text cart-qt-btn-plus"><i class="fa fa-plus"></i></span>

                                        </div>
                                        <span v-if="true" class="form-control cart-qt-text">{{getAProduct(<?php echo collect($product)->toJson() ?>).qt ?? 0}}</span>

                                        <div class="input-group-append">
                                            <span v-on:click="remoceQtCart({{ collect($product)->toJson() }})" class="input-group-text cart-qt-btn-minus"><i class="fa fa-minus"></i></span>
                                        </div>
                                    </div>




                            </div>
                            <div class="add-to_cart  col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8  ">

                                <div class=" row col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 btn-group">

                                    <a class="btn R-Mart-button-custom primary-btn  col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"  v-if="!productExistInCart({{ collect($product)->toJson() }})" v-on:click="addToCart({{ collect($product)->toJson() }})">Add to cart</a>
                                    <a class="btn R-Mart-button-custom primary-btn  col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"  v-if="productExistInCart({{ collect($product)->toJson() }})" ><i class="fa fa-check"></i> Added to cart</a>
                                    <a class="btn R-Mart-button-2-custom primary-btn  col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"  v-on:click="butitnow({{ collect($product)->toJson() }},'{{route('cart')}}')">Buy it Now</a>
{{--                                    <a class="btn R-Mart-button-2-custom treansparent-color col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 " href="wishlist.html">Add to wishlist</a>--}}


                                </div>

                            </div>
                        </div>

                        <div class="social-share mb-4"> <span>Share :</span>


                            <a href="https://www.facebook.com/sharer/sharer.php?u={{$product['view_url']}}&t={{$product['pname']}}"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                               target="_blank" title="Share on Facebook"><i class="fa fa-facebook-square facebook-color"></i></a>


                            <a href="https://twitter.com/intent/tweet?url={{$product['view_url']}}"
                            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                            target="_blank" title="Share on Twitter"><i class="fa fa-twitter-square twitter-color"></i></a>
                            <a href="https://www.linkedin.com/shareArticle?mini=true&url={{$product['view_url']}}"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                               target="_blank" title="Share on Linkedin" ><i class="fa fa-linkedin-square linkedin-color"></i></a>
                            <a
                               href="http://pinterest.com/pin/create/link/?url={{$product['view_url']}}&title={{$product['pname']}}&description={{urlencode($product['psdesc'])}}"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                               target="_blank" title="Share on Pinterest"

                            ><i class="fa fa-pinterest-square pinterest-color"></i></a>
                        </div>
                        <div class="payment mb-2"> <small> Available Payments Methods</small></div>
                        <div class="payment"> <a href="#"><img class="border" src="{{asset('assets/images/payment/img-payment.png')}}" alt="Payment"></a> </div>

                        <div v-cloak v-if="true" class="product-area ">
                            <div class="container container-default custom-area">

                                <div class="row mt-3 pl-2 block text-center">

                                    <h4 class="text-center ">Relates Products</h4>

                                </div>

                                <div class="row">
                                    <div class="col-lg-12 product-wrapper col-custom">
                                        <div class="product-slider small" data-slick-options='{
                        "slidesToShow": 4,
                        "slidesToScroll": 1,
                        "infinite": true,
                        "arrows": false,
                        "dots": false
                        }' data-slick-responsive='[
                        {"breakpoint": 1200, "settings": {
                        "slidesToShow": 3
                        }},
                        {"breakpoint": 992, "settings": {
                        "slidesToShow": 2
                        }},
                        {"breakpoint": 576, "settings": {
                        "slidesToShow": 1
                        }}
                        ]'>
                                            @php
                                            $relatedProducts=[];

                                            $model=\App\Models\ProductModel::all()->take(5) ;
                                            if($model->count()>0){
                                            $relatedProducts=$model->toArray();
                                            }
                                            @endphp



                                            @foreach($relatedProducts as $p)
                                                @if($p['id'] != $product['id'])
                                            <div class="single-item">
                                                <div class="single-product single-product-small position-relative">
                                                    <div class="product-image product-image-small"> <a class="d-block" href="{{ $p['view_url'] }}"> <img src="{{implode('',[$p['base_url'],$p['pimg']])}}" alt="" class="product-image-1 w-100"> <img src="{{implode('',[$p['base_url'],(count($p['pimgs'])>0)?$p['pimgs'][0]:''])}}" alt="" class="product-image-2 position-absolute w-100"> </a> </div>
                                                    <div class="product-content product-content-small">
                                                        <div class="product-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                        <div class="product-title">
                                                            <h4 class="title-2"> <a href="product-details.html">{{$p['pname']}} <span v-if="productExistInCart({{ collect($p)->toJson() }})">( {{  getAProduct(<?php echo collect($p)->toJson() ?>).qt??0}}  )</span></a></h4>
                                                        </div>
                                                        <div class="price-box"> <span class="regular-price ">₹ {{ number_format($p['default_price'] -( ($p['discount']/100)*$p['default_price']),2)  }}</span> <span class="old-price text-danger"><del>₹ {{ number_format($p['default_price'],2)  }}</del></span> </div>
                                                    </div>
                                                    <div class="add-action add-action-small d-flex position-absolute">
                                                        <a v-if="!productExistInCart({{ collect($p)->toJson() }})" v-on:click="addToCart({{ collect($p)->toJson() }})" title="Add To cart"> <i class="ion-bag"></i></a>
                                                        <a v-if="productExistInCart({{ collect($p)->toJson() }})" title="Product Exist"  >


                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span v-on:click="addToCart({{ collect($p)->toJson() }})" class="input-group-text cart-qt-btn-plus"><i class="fa fa-plus"></i></span>

                                                                </div>
                                                                <span v-if="false" class="form-control cart-qt-text">{{   getAProduct(<?php echo collect($p)->toJson() ?>).qt??0}}</span>
                                                                {{--                            <input type="number" :value="" class="form-control cart-qt-text" aria-label="Amount (to the nearest dollar)">--}}
                                                                <div class="input-group-append">
                                                                    <span v-on:click="remoceQtCart({{ collect($p)->toJson() }})" class="input-group-text cart-qt-btn-minus"><i class="fa fa-minus"></i></span>
                                                                </div>
                                                            </div>





                                                        </a>
                                                        <a v-if="productExistInCart({{ collect($p)->toJson() }})">
                                                            <div class="btn btn-outline-danger  cart-delete-product" v-on:click="deleteFromCart({{ collect($p)->toJson() }})"><i class="fa fa-trash-o"></i> </div>
                                                        </a>
                                                        <a v-if="false" href="compare.html" title="Compare"> <i class="ion-ios-loop-strong"></i> </a>
                                                        <a v-if="false" href="wishlist.html" title="Add To Wishlist"> <i class="ion-ios-heart-outline"></i> </a>
                                                        <a href="{{$p['view_url']}}" class="cart-view-btn" title="Quick View"> <i class="ion-eye  "></i> </a>
                                                    </div>
                                                </div>
                                            </div>
                                                @endif
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-no-text">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs justify-content-around" id="myTab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active text-uppercase" id="home-tab" data-toggle="tab" href="#connect-1" role="tab" aria-selected="true">Description</a> </li>
                        <span v-cloak v-if="false">
                        <li class="nav-item"> <a clss="nav-link text-uppercase" id="profile-tab" data-toggle="tab" href="#connect-2" role="tab" aria-selected="false" >Reviews</a> </li>
                        </span>
                        <li class="nav-item"> <a class="nav-link text-uppercase" id="contact-tab" data-toggle="tab" href="#connect-3" role="tab" aria-selected="false">Shipping Policy</a> </li>
                        @if(is_array($product['price']) && count($product['price'])>1)
                        <li class="nav-item"> <a class="nav-link text-uppercase" id="review-tab" data-toggle="tab" href="#connect-4" role="tab" aria-selected="false">Size Chart</a> </li>
                        @endif
                    </ul>
                    <div class="tab-content mb-text" id="myTabContent">
                        <div class="tab-pane fade show active" id="connect-1" role="tabpanel" aria-labelledby="home-tab" >
                            <div class="desc-content">
                                <p class="mb-3" >       {!! $product['psdesc'] !!}</p>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="connect-2" role="tabpanel" aria-labelledby="profile-tab" v-cloak v-if="false">
                            <!-- Start Single Content -->
                            <div class="product_tab_content  border p-3">
                                <div class="review_address_inner">
                                    <!-- Start Single Review -->
                                    <div class="pro_review mb-5">
                                        <div class="review_thumb"> <img alt="review images" src="{{asset  ('assets/images/review/1.jpg')}}"> </div>
                                        <div class="review_details">
                                            <div class="review_info mb-2">
                                                <div class="product-rating mb-2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                <h5>Admin - <span> December 19, 2020</span></h5>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in viverra ex, vitae vestibulum arcu. Duis sollicitudin metus sed lorem commodo, eu dapibus libero interdum. Morbi convallis viverra erat, et aliquet orci congue vel. Integer in odio enim. Pellentesque in dignissim leo. Vivamus varius ex sit amet quam tincidunt iaculis.</p>
                                        </div>
                                    </div>
                                    <!-- End Single Review -->
                                </div>
                                <!-- Start RAting Area -->
                                <div class="rating_wrap">
                                    <h5 class="rating-title-1 mb-2">Add a review </h5>
                                    <p class="mb-2">Your email address will not be published. Required fields are marked *</p>
                                    <h6 class="rating-title-2 mb-2">Your Rating</h6>
                                    <div class="rating_list mb-4">
                                        <div class="review_info">
                                            <div class="product-rating mb-3"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End RAting Area -->
                                <div class="comments-area comments-reply-area">
                                    <div class="row">
                                        <div class="col-lg-12 col-custom">
                                            <form action="#" class="comment-form-area">
                                                <div class="row comment-input">
                                                    <div class="col-md-6 col-custom comment-form-author mb-3">
                                                        <label>Name <span class="required">*</span></label>
                                                        <input type="text" required="required" name="Name">
                                                    </div>
                                                    <div class="col-md-6 col-custom comment-form-emai mb-3">
                                                        <label>Email <span class="required">*</span></label>
                                                        <input type="text" required="required" name="email">
                                                    </div>
                                                </div>
                                                <div class="comment-form-comment mb-3">
                                                    <label>Comment</label>
                                                    <textarea class="comment-notes" required="required"></textarea>
                                                </div>
                                                <div class="comment-form-submit">
                                                    <input type="submit" value="Submit" class="comment-submit btn R-Mart-button primary-btn">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Content -->
                        </div>
                        <div class="tab-pane fade" id="connect-3" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="shipping-policy">
                                <h4 class="title-3 mb-4">Shipping policy for our store</h4>
                                <p class="desc-content mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
                                <ul class="policy-list mb-2">
                                    <li>1-2 business days (Typically by end of day)</li>
                                    <li><a href="#">30 days money back guaranty</a></li>
                                    <li>24/7 live support</li>
                                    <li>odio dignissim qui blandit praesent</li>
                                    <li>luptatum zzril delenit augue duis dolore</li>
                                    <li>te feugait nulla facilisi.</li>
                                </ul>
                                <p class="desc-content mb-2">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum</p>
                                <p class="desc-content mb-2">claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per</p>
                                <p class="desc-content mb-2">seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                            </div>
                        </div>
                        <div id="product-details-more"></div>
                        @if(is_array($product['price']) && count($product['price'])>1)

                        <div class="tab-pane fade" id="connect-4" role="tabpanel" aria-labelledby="review-tab">
                            <div class="size-tab table-responsive-lg">
                                <h4 class="title-3 mb-4">Size Chart</h4>
                                <table class="table border">
                                    <tbody>
                                    <tr>
                                        <td class="cun-name"><span>UK</span></td>
                                        <td>18</td>
                                        <td>20</td>
                                        <td>22</td>
                                        <td>24</td>
                                        <td>26</td>
                                    </tr>
                                    <tr>
                                        <td class="cun-name"><span>European</span></td>
                                        <td>46</td>
                                        <td>48</td>
                                        <td>50</td>
                                        <td>52</td>
                                        <td>54</td>
                                    </tr>
                                    <tr>
                                        <td class="cun-name"><span>usa</span></td>
                                        <td>14</td>
                                        <td>16</td>
                                        <td>18</td>
                                        <td>20</td>
                                        <td>22</td>
                                    </tr>
                                    <tr>
                                        <td class="cun-name"><span>Australia</span></td>
                                        <td>28</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <td class="cun-name"><span>Canada</span></td>
                                        <td>24</td>
                                        <td>18</td>
                                        <td>14</td>
                                        <td>42</td>
                                        <td>36</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Single Product Main Area End -->

@endsection
