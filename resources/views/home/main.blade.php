@extends('layout.root')


@section('body')
@include('home.partials.slider')
@include('home.partials.product')
@include('home.partials.banner')
@include('home.partials.product_area')
@include('home.partials.feature_1')
@include('home.partials.feature_2')
@include('home.partials.newslatter')
@include('home.partials.calltoaction')
@include('home.partials.support')
@endsection







