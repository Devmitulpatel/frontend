@extends('layout.root',['active'=>'shop'])


@section('body')


    @php

    $model=new \App\Models\CatModel();
    $model=$model->all();

    $catsWithSub= ($model->count()>0)?$model->toJson():collect([])->toJson();
    $pmodel=new \App\Models\ProductModel ();
    $pGet=$pmodel->orderBy('id','desc');
    $lastesProduct=$pGet->take(5)->get()->toJson();
    $productData=$pGet->paginate(12)->toJson();

 //   dd($productData);

    @endphp


    <!-- Breadcrumb Area Start Here -->
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Shop</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Shop</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->

    <shop-front inline-template :categories="{{$catsWithSub}}" :latest-product="{{$lastesProduct}}" :start-product-data="{{$productData}}" :data-url="'{{route('shopData')}}'">
    <!-- Shop Main Area Start Here -->
    <div class="shop-main-area">
        <div class="container container-default custom-area">
            <div class="row flex-row-reverse">
                <div class="col-lg-9 col-12 col-custom widget-mt">
                    <!--shop toolbar start-->
                    <div class="shop_toolbar_wrapper">
                        <div class="shop_toolbar_btn">
                            <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip" title="3"><i class="fa fa-th"></i></button>
                            <!-- <button data-role="grid_4" type="button"  class=" btn-grid-4" data-toggle="tooltip" title="4"></button> -->
                            <button data-role="grid_list" type="button" class="btn-list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        </div>
                        <div class="shop-select">
                            <form class="d-flex flex-column w-100" action="#">
                                <div class="form-group">
                                    <select class="form-control nice-select w-100">
                                        <option selected value="1">Alphabetically, A-Z</option>
                                        <option value="2">Sort by popularity</option>
                                        <option value="3">Sort by newness</option>
                                        <option value="4">Sort by price: low to high</option>
                                        <option value="5">Sort by price: high to low</option>
                                        <option value="6">Product Name: Z</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <!-- Shop Wrapper Start -->
                    <div class="row shop_wrapper grid_3" v-cloak>

                        <div class="col-md-6 col-sm-6 col-lg-4 col-custom product-area" v-for="p in productCollection" >
                            <div class="single-product position-relative">

                                <div class="product-image"> <a class="d-block" :href="p.view_url">
                                        <img :src="p.base_url+p.pimg" alt="" class="product-image-1 w-100">
                                        <img :src="p.base_url+ ((p.pimgs.length > 0 )?p.pimgs[0]:p.pimg)" alt="" class="product-image-2 position-absolute w-100">

                                    </a> </div>
                                <div class="product-content" :title="p.psdesc">
                                    <div class="product-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                    <div class="product-title">
                                        <h4 class="title-2"> <a :href="p.view_url">@{{ p.pname }}</a> <span v-if="productExistInCart(p)">( @{{getAProduct(p).qt}} )</span></h4>
                                    </div>
                                    <div class="price-box"> <span class="regular-price ">₹ @{{ p.default_price-((p.discount/100)*p.default_price) }}</span> <span v-cloak v-if="p.discount>0" class="old-price text-danger"><del>₹ @{{ p.default_price }}</del></span> </div>
                                </div>

                                <div class="add-action d-flex position-absolute">
                                    <a v-if="!productExistInCart(p)" v-on:click="addToCart(p)" title="Add To cart"> <i class="ion-bag"></i></a>
                                    <a v-if="productExistInCart(p)" title="Product Exist"  >


                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span v-on:click="addToCart(p)" class="input-group-text cart-qt-btn-plus"><i class="fa fa-plus"></i></span>

                                            </div>
                                            <span v-if="false" class="form-control cart-qt-text">@{{getAProduct(p).qt}}</span>
                                            {{--                            <input type="number" :value="" class="form-control cart-qt-text" aria-label="Amount (to the nearest dollar)">--}}
                                            <div class="input-group-append">
                                                <span v-on:click="remoceQtCart(p)" class="input-group-text cart-qt-btn-minus"><i class="fa fa-minus"></i></span>
                                            </div>
                                        </div>





                                    </a>
                                    <a v-if="productExistInCart(p)">
                                        <div class="btn btn-outline-danger  cart-delete-product" v-on:click="deleteFromCart(p)"><i class="fa fa-trash-o"></i> </div>
                                    </a>
                                    <a v-if="false" href="compare.html" title="Compare"> <i class="ion-ios-loop-strong"></i> </a>
                                    <a v-if="false" href="wishlist.html" title="Add To Wishlist"> <i class="ion-ios-heart-outline"></i> </a>
                                    <a :href="p.view_url" > <i class="ion-eye"></i> </a>
                                </div>

                                <div class="product-content-listview">
                                    <div class="product-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                    <div class="product-title">
                                        <h4 class="title-2"> <a href="">@{{ p.pname }}</a></h4>
                                        <small class="title-3"> <a href="">Qt: <strong v-if="productExistInCart(p)"> @{{getAProduct(p).qt}} </strong></a></small>
                                    </div>
                                    <div class="price-box"> <span class="regular-price ">₹ @{{ p.default_price-((p.discount/100)*p.default_price) }}</span> <span v-cloak v-if="p.discount>0" class="old-price text-danger"><del>₹ @{{ p.default_price }}</del></span> </div>
                                    <div class="add-action-listview d-flex">
                                        <a v-if="!productExistInCart(p)" v-on:click="addToCart(p)" title="Add To cart"> <i class="ion-bag"></i></a>
                                        <a v-if="productExistInCart(p)" title="Product Exist"  >


                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span v-on:click="addToCart(p)" class="input-group-text cart-qt-btn-plus"><i class="fa fa-plus"></i></span>

                                                </div>
                                                <span v-if="false" class="form-control cart-qt-text">@{{getAProduct(p).qt}}</span>
                                                {{--                            <input type="number" :value="" class="form-control cart-qt-text" aria-label="Amount (to the nearest dollar)">--}}
                                                <div class="input-group-append">
                                                    <span v-on:click="remoceQtCart(p)" class="input-group-text cart-qt-btn-minus"><i class="fa fa-minus"></i></span>
                                                </div>
                                            </div>





                                        </a>
                                        <a v-if="productExistInCart(p)">
                                            <div class="btn btn-outline-danger  cart-delete-product" v-on:click="deleteFromCart(p)"><i class="fa fa-trash-o"></i> </div>
                                        </a>
                                        <a v-if="false" href="compare.html" title="Compare"> <i class="ion-ios-loop-strong"></i> </a>
                                        <a v-if="false" href="wishlist.html" title="Add To Wishlist"> <i class="ion-ios-heart-outline"></i> </a>
                                        <a :href="p.view_url"  title="Quick View"> <i class="ion-eye"></i> </a>
                                    </div>
                                    <p class="desc-content">

                                        @{{ p.psdesc }}

                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-lg-12 " v-if="productCollection.length<1" v-cloak>
                            <div class="no-data-found">
                                <img  src="{{asset('imgs/nodata.png')}}">
                                <span>Opps...<br> No Product Found </span>
                                <div class="btn btn-block btn-lg" v-on:click="resetProduct"> <i class="fa fa-magic"></i> See other Products</div>
                            </div>

                        </div>

                    </div>
                    <!-- Shop Wrapper End -->
                    <!-- Bottom Toolbar Start -->
                    <div class="row">
                        <div class="col-sm-12 col-custom" v-if="productCollection.length>0">
                            <div class="toolbar-bottom mt-30">
                                <nav class="pagination pagination-wrap mb-10 mb-sm-0">
                                    <ul class="pagination" v-cloak>
                                        <li class="prev" :class="{ 'disabled':pageData.current_page < 2 }" v-on:click="previosPage"> <i class="ion-ios-arrow-thin-left"></i> </li>
                                        <li class="pagination-page-btn" v-if="(pageData.current_page)>3" v-on:click="changePage(1)">1</li>
                                        <li v-if="(pageData.current_page)>4 ">...</li>
                                        <li class="pagination-page-btn" v-for="page in pageData.last_page" v-if="(page<=pageData.current_page+2 && page>=pageData.current_page-2) || pageData.last_page<3 " v-on:click="changePage(page)" :class="{ 'active':page==pageData.current_page}"> @{{ page }}</li>
                                        <li v-if="(pageData.last_page-pageData.current_page )>3">...</li>
                                        <li class="pagination-page-btn" v-if="(pageData.last_page-pageData.current_page )>4" v-on:click="changePage(pageData.last_page)">@{{ pageData.last_page }}</li>
                                        <li v-on:click="nextPage" class="next" :class="{ 'disabled':pageData.last_page<2 || pageData.last_page==pageData.current_page}" > <a href="#" title="Next >>"><i class="ion-ios-arrow-thin-right"></i></a> </li>
                                    </ul>
                                </nav>
                                <p class="desc-content text-center text-sm-right">Showing @{{ pageData.from }} - @{{ pageData.to }} of @{{ pageData.total }} result</p>
                            </div>
                        </div>
                    </div>
                    <!-- Bottom Toolbar End -->
                </div>
                <div class="col-lg-3 col-12 col-custom">
                    <!-- Sidebar Widget Start -->
                    <aside class="sidebar_widget widget-mt">
                        <div class="widget_inner">
                            <div class="widget-list widget-mb-1">
                                <h3 class="widget-title">Search</h3>
                                <div class="search-box">
                                    <div class="input-group">
                                        <input v-on:keypress.enter="searchProduct"  type="text" v-model="search_query" class="form-control" placeholder="Search Our Store" aria-label="Search Our Store">
                                        <div class="input-group-append">
                                            <button v-on:click="searchProduct" class="btn btn-outline-secondary" type="button"> <i class="fa fa-search"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-list widget-mb-1">
                                <h3 class="widget-title">Menu Categories</h3>
                                <!-- Widget Menu Start -->
                                <nav>

                                    <ul class="mobile-menu p-0 m-0" v-for="cat in currentCat" >


                                    </ul>
                                    <ul class="mobile-menu p-0 m-0">


                                        <li class="menu-item-has-children"  v-for="cat in currentCat">
                                            <a href="#">@{{ cat.catname }} (@{{ cat.subs.length }}) </a>
                                            <ul class="dropdown" v-if="cat.subs.length>0">
                                                <li v-for="subcat in cat.subs" v-on:click="filterByCatNSubCat(cat.id,subcat.id)"><a href="vue">@{{ subcat.name }} <span class="subcat-no">(@{{ subcat.products_count }})</span></a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </nav>
                                <!-- Widget Menu End -->
                            </div>

                            <div class="widget-list widget-mb-4">
                                <h3 class="widget-title">Recent Products</h3>
                                <div class="sidebar-body">


                                    <div class="sidebar-product align-items-center" v-for="p in latestProduct">

                                        <a :href="p.view_url" class="image"> <img :src="p.base_url+p.pimg" alt="product"> </a>

                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2"> <a :href="p.view_url">@{{ p.pname }}</a></h4>
                                            </div>
                                            <div class="price-box"> <span class="regular-price ">₹ @{{ parseFloat(p.default_price-((p.discount/100)*p.default_price)).toFixed(2) }}</span> <span v-if="p.discount>0" class="old-price text-danger"><del>₹ @{{ parseFloat(p.default_price).toFixed(2) }}</del></span> </div>
                                            <div class="product-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </aside>
                    <!-- Sidebar Widget End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Main Area End Here -->

    </shop-front>



@endsection
