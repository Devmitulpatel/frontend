<!-- Feature Area Start Here -->
<div class="feature-area mb-no-text">
    <div class="container container-default custom-area">
        <div class="row align-items-center">
            <div class="col-xl-5 col-lg-7 col-md-7 col-custom order-2 order-md-1">
                <div class="feature-image position-relative"> <img class="w-100" src="assets/images/feature/feature-3.png" alt="R-Mart Feature"> </div>
            </div>
            <div class="col-xl-7 col-lg-5 col-md-5 col-custom order-1 order-md-2">
                <div class="feature-content-countdown">
                    <h2 class="title">8. Countdown Product</h2>
                    <div class="price-box"> <span class="regular-price">$80.00</span> <span class="old-price"><del>$90.00</del></span> </div>
                    <p class="desc-content"> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. </p>
                    <div class="countdown-wrapper countdown-style-2 d-flex" data-countdown="2022/12/24"></div>
                    <a href="shop.html" class="btn R-Mart-button-2 primary-color">Shop Now</a> </div>
            </div>
        </div>
    </div>
</div>
<!-- Feature Area End Here -->
