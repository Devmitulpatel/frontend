<!-- Begin Slider Area One -->
<div class="slider-area">
    <div class="R-Mart-slider arrow-style" data-slick-options='{
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "infinite": true,
        "arrows": true,
        "dots": true,
        "autoplay" : true,
        "fade" : true,
        "autoplaySpeed" : 7000,
        "pauseOnHover" : false,
        "pauseOnFocus" : false
        }' data-slick-responsive='[
        {"breakpoint":992, "settings": {
        "slidesToShow": 1,
        "arrows": false,
        "dots": true
        }}
        ]'>
        <div class="slide-item slide-1 bg-position slide-bg-2 animation-style-01">
            <div class="slider-content">
                <h4 class="slider-small-title">Organic Products</h4>
                <h2 class="slider-large-title">Life with Beauty</h2>
                <div class="slider-btn"> <a class="R-Mart-button black-btn" href="{{route('shop')}}">Shop Now</a> </div>
            </div>
        </div>
        <div class="slide-item slide-4 bg-position slide-bg-2 animation-style-01">
            <div class="slider-content">
                <h4 class="slider-small-title">Cold process organic</h4>
                <h2 class="slider-large-title">Superior skin care</h2>
                <div class="slider-btn"> <a class="R-Mart-button black-btn" href="{{route('shop')}}">Shop Now</a> </div>
            </div>
        </div>
    </div>
</div>
<!-- Slider Area One End Here -->
