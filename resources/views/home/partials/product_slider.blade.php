<div class="product-slider @if(isset($small) && $small) small @endif "
     data-slick-options='{

                    "slidesToShow": 4,
                    "slidesToScroll": 1,
                    "infinite": true,
                    "arrows": false,
                    "dots": false
                    }'
     data-slick-responsive='[
                    {"breakpoint": 1200, "settings": {
                    "slidesToShow": 3
                    }},
                    {"breakpoint": 992, "settings": {
                    "slidesToShow": 2
                    }},
                    {"breakpoint": 576, "settings": {
                    "slidesToShow": 1
                    }}
                    ]'>


    @foreach($products as $p)

        @php

            $eP=explode('$;',$p['pprice']);
            $eU=explode('$;',$p['pgms']);
            $price=(float)$p['default_price'];
            $unit=$p['default_unit'];
            $rprice=$price-( ($p['discount']/100)*$price );
        @endphp

        <div class="single-item">
            <div class="single-product position-relative" style="min-height: 474.43px!important;">
                <div class="product-image"><a class="d-block" href="{{$p['view_url']}}">
                        <img
                            style="min-height: 354px" src="{{implode('',[$p['base_url'],$p['pimg']])}}"  alt=""
                            class="product-image-1 w-100">
                        <img style="min-height: 354px"

                             src="{{implode('',[$p['base_url'], (count($p['pimgs'])>0)?reset($p['pimgs']):$p['pimg']])}}" alt=""
                             class="product-image-2 position-absolute w-100"> </a>
                </div>
                <div class="product-content">
                    <div class="product-rating" >
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i></div>
                    <div class="product-title">
                        <h4 class="title-2"><a href="{{$p['view_url']}}">{{$p['pname']}} </a> <span v-if="productExistInCart({{ collect($p)->toJson() }})">( {{getAProduct(<?php echo collect($p)->toJson() ?>).qt}} )</span> </h4>
                    </div>
                    <div class="price-box">
                        <span class="regular-price ">{{ implode(' ',[$c,number_format($rprice,2)])  }} </span>
                        @if($p['discount']>0)
                            <span class="old-price text-danger"><del>{{ implode(' ',[$c,number_format($price,2)])  }}</del></span>
                        @endif
                        <small style="font-weight: 200">/ {{$unit}}</small>
                    </div>
                </div>
                <div class="add-action d-flex position-absolute mt-2">

                    <a v-if="!productExistInCart({{ collect($p)->toJson() }})" v-on:click="addToCart({{ collect($p)->toJson() }})" title="Add To cart"> <i class="ion-bag"></i></a>
                    <a v-if="productExistInCart({{ collect($p)->toJson() }})" title="Product Exist"  >


                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span v-on:click="addToCart({{ collect($p)->toJson() }})" class="input-group-text cart-qt-btn-plus"><i class="fa fa-plus"></i></span>

                            </div>
                            <span v-if="false" class="form-control cart-qt-text">{{getAProduct(<?php echo collect($p)->toJson() ?>).qt}}</span>
{{--                            <input type="number" :value="" class="form-control cart-qt-text" aria-label="Amount (to the nearest dollar)">--}}
                            <div class="input-group-append">
                                <span v-on:click="remoceQtCart({{ collect($p)->toJson() }})" class="input-group-text cart-qt-btn-minus"><i class="fa fa-minus"></i></span>
                            </div>
                        </div>





                    </a>
                    <a v-if="productExistInCart({{ collect($p)->toJson() }})">
                        <div class="btn btn-outline-danger  cart-delete-product" v-on:click="deleteFromCart({{ collect($p)->toJson() }})"><i class="fa fa-trash-o"></i> </div>
                    </a>
                    <a v-if="false" href="compare.html" title="Compare"> <i class="ion-ios-loop-strong"></i> </a>
                    <a v-if="false" href="wishlist.html" title="Add To Wishlist"> <i class="ion-ios-heart-outline"></i> </a>
                    <a href="{{$p['view_url']}}" title="Quick View"> <i class="ion-eye"></i> </a>
                </div>
            </div>

        </div>

    @endforeach

</div>
