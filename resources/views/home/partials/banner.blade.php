<!-- Banner Area Start Here -->
<div class="banner-area">
    <div class="container container-default custom-area">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-custom">
                <div class="banner-image hover-style"> <a class="d-block" href="{{route('shop')}}"> <img class="w-100" src="assets/images/banner/small-banner/friuts-banner.jpg" alt="Banner Image"> </a> </div>
            </div>
            <div class="col-md-6 col-sm-12 col-custom">
                <div class="banner-image hover-style mb-0"> <a class="d-block" href="{{route('shop')}}"> <img class="w-100" src="assets/images/banner/small-banner/vege-banner.jpg" alt="Banner Image"> </a> </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner Area End Here -->
