<!-- Call To Action Area Start Here -->
<div class="call-to-action-area mb-text">
    <div class="container container-default custom-area">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-custom">
                <div class="call-to-action-item mt-0 d-lg-flex d-md-block align-items-center">
                    <div class="call-to-action-icon"> <img src="assets/images/icons/icon-1.png" alt="Icon"> </div>
                    <div class="call-to-action-info">
                        <h3 class="action-title">Free Home Delivery</h3>
                        <p class="desc-content">Provide free home delivery for all product over $100</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-custom">
                <div class="call-to-action-item d-lg-flex d-md-block align-items-center">
                    <div class="call-to-action-icon"> <img src="assets/images/icons/icon-2.png" alt="Icon"> </div>
                    <div class="call-to-action-info">
                        <h3 class="action-title">Quality Products</h3>
                        <p class="desc-content">We ensure our product quality all times</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-custom">
                <div class="call-to-action-item d-lg-flex d-md-block align-items-center">
                    <div class="call-to-action-icon"> <img src="assets/images/icons/icon-3.png" alt="Icon"> </div>
                    <div class="call-to-action-info">
                        <h3 class="action-title">Online Support</h3>
                        <p class="desc-content">To satisfy our customer we try to give support online</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Call to Action Area End Here -->
