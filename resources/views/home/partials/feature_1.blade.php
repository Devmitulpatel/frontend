<!-- Feature Area Start Here -->
<div class="feature-area mb-no-text">
    <div class="container container-default custom-area">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-5 col-md-12 col-custom">
                <div class="feature-content-wrapper">
                    <h2 class="title">Important to eat fruit</h2>
                    <p class="desc-content">Eating fruit provides health benefits — people who eat more fruits and vegetables as part of an overall healthy diet are likely to have a reduced risk of some chronic diseases. Fruits provide nutrients vital for health and maintenance of your body.</p>
                    <p class="desc-content"> Fruits are sources of many essential nutrients that are underconsumed, including potassium, dietary fiber, vitamin C, and folate (folic acid).</p>
                    <p class="desc-content"> Most fruits are naturally low in fat, sodium, and calories. None have cholesterol.</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7 col-md-12 col-custom">
                <div class="feature-image position-relative"> <img src="assets/images/feature/feature-1.jpg" alt="R-Mart Feature">
                    <div class="popup-video position-absolute"> <a class="popup-vimeo" href="https://www.youtube.com/"> <i class="ion-play"></i> </a> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Feature Area End Here -->
