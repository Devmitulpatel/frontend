<!-- Product Area Start Here -->
<div class="product-area mt-text mb-no-text">
    <div class="container container-default custom-area">
        <div class="row">
            <div class="col-lg-5 col-custom m-auto text-center">
                <div class="section-content">
                    <h2 class="title-1 text-uppercase">Featured Products</h2>
                    <div class="desc-content">
                        <p>All best seller product are now available for you and your can buy this product from here any time any where so sop now</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product-wrapper col-lg-12 col-custom">


                @include('home.partials.product_slider',
                    [
                        'products'=>App\Models\ProductModel::where(['popular'=>1])->take(10)->get()->toArray(),
                        'backend'=>'http://b.rmart.m',
                        'c'=>'₹'
                    ] )


            </div>
        </div>
    </div>
</div>
<!-- Product Area End Here -->
