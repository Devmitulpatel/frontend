<!-- Product Area Start Here -->
<div class="product-area mt-text mb-text-p">
    <div class="container container-default custom-area">
        <div class="row">
            <div class="col-lg-5 m-auto text-center col-custom">
                <div class="section-content">
                    <h2 class="title-1 text-uppercase">Best Sale</h2>
                    <div class="desc-content">
                        <p>All best seller product are now available for you and your can buy this product from here any time any where so sop now</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 product-wrapper col-custom">


                @include('home.partials.product_slider',
              [
                  'products'=>App\Models\ProductModel::all()->skip(10)->take(10)->toArray(),
                  'backend'=>'http://b.rmart.m',
                  'c'=>'₹'
              ] )


            </div>
        </div>
    </div>
</div>
<!-- Product Area End Here -->
